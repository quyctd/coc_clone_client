
var AnimBase = cc.Sprite.extend({

    _sprite: null,

    ctor: function() {
        this._super();
    },

    create: function (building) {
        this.count = 0;
        this.buildSize = building.buildSize;
        //add shadow
        var strFileShadow = "#GRASS_"+ building.buildSize +"_Shadow.png";
        this.shadow = cc.Sprite(strFileShadow);
        this.addChild(this.shadow, 1);

        //load idle
        var rootIdle = resIdle[building.uid] + building.uid +"_" + building.level + "/idle.plist";
        cc.spriteFrameCache.addSpriteFrames(rootIdle);
        this.idle = cc.Sprite("#image0000.png");
        this.addChild(this.idle, 2);
        cc.spriteFrameCache.removeSpriteFramesFromFile(rootIdle);

        if (building.uid in resAni) {
            var strRoot = resAni[building.uid] + building.uid + "_" + building.level + "_effect/";

            if (building.uid === "AMC_1" && building.level >= 2)
                return;

            cc.spriteFrameCache.addSpriteFrames(strRoot + "anim.plist");
            // Animation
            var plist = cc.loader.getRes(strRoot + "anim.plist");
            var lenOfPlist = Object.keys(plist['frames']).length;
            var animFrames = [];
            var str = "";
            var frame;
            for (var i = 0; i < lenOfPlist; i++) {
                if (i < 10)  str = "0" + i+ ".png";
                else str = i + ".png";
                frame = cc.spriteFrameCache.getSpriteFrame(str);
                animFrames.push(frame);
            }
            var animation = new cc.Animation(animFrames, 0.15);
            this._sprite = new cc.Sprite();
            this._sprite.runAction(cc.animate(animation).repeatForever());
            this.addChild(this._sprite, 3);
            cc.spriteFrameCache.removeSpriteFramesFromFile(strRoot+"anim.plist");

        }
    },

    runEffect: function(bool) {
        var seqRep = cc.sequence(cc.TintTo(0.5, 202, 204, 206), cc.TintTo(0.5, 255, 255, 255)).repeatForever();
        if (bool) {
            var seqScale = cc.sequence(cc.scaleTo(0.2, 1.2), cc.scaleTo(0.2, 1));
            this.idle.runAction(seqScale);
            this.idle.runAction(seqRep);
        }
        else {
            this.idle.stopAllActions();
            this.idle.color = new cc.Color(255, 255, 255);
        }
    },

    drawUpgrade: function(stateBuild, timeBuild, percent) {

         if (stateBuild !== BUILDING_STATUS.BUILDING_FREE) { //upgrade
            //build time
            //Add Cọc
            var pile = cc.Sprite("#upgrading.png");
            pile.setAnchorPoint(0.5, 0);
            pile.y -=  this.buildSize * BUILDINGBASEGRID_HEIGHT/2;
            this.addChild(pile, zorder.z_anim);

            //Add build slide
            //bg train bar
            cc.spriteFrameCache.addSpriteFrames(res.TrainTroopUI_plist);
            var loadingBar = cc.Sprite("#bg_train_bar.png");
            loadingBar.scale = 2;
            loadingBar.y += this.buildSize * BUILDINGBASEGRID_HEIGHT/2 - loadingBar.height/2;
            this.addChild(loadingBar, zorder.z_loading);

            //add train bar
            this.trainBar = new ccui.LoadingBar();
            this.trainBar.setName("TrainBar");
            this.trainBar.scale = 2;
            this.trainBar.loadTexture("train_bar.png", ccui.Widget.PLIST_TEXTURE);
            this.trainBar.setAnchorPoint(0, 0.5);
            this.trainBar.x = loadingBar.x - loadingBar.width/2 * loadingBar.scale;
            this.trainBar.y = loadingBar.y;
            this.trainBar.setPercent(percent);
            this.addChild(this.trainBar, zorder.z_trainBar);

            //add Time build
            this.timeLabel = new cc.LabelBMFont(timeBuild, "res/Fonts/soji_20.fnt");
            this.timeLabel.y = this.trainBar.y + this.trainBar.height/2 + this.timeLabel.height/2;
            this.addChild(this.timeLabel, zorder.z_trainBar);

             cc.spriteFrameCache.removeSpriteFramesFromFile(res.TrainTroopUI_plist);
        }

    },

    judgeDirection: function(angle) {

    },

    setAction: function (actionID) {

    },

    isHaveAction: function(actionID) {

    },

    getAction: function (direction) {

    },

    restartAction: function (action) {
        
    },

    changeAction: function (action) {
        
    }

});