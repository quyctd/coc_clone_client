/**
 * Created by CPU02302_LOCAL on 12/7/2018.
 */

var BaseBuilding = Unit.extend({

    buildingSubstrateBottom: null,
    arrowSprite: null,
    showGreenSubstrate: true,
    mapIndex: null,
    uid: null,
    b_ID: null,
    buildSize: null,
    level:0,
    capacity_current: 0,
    product_rate: 0,

    ctor: function() {
        this._super();
    },

    init: function(uuid, BID, buildSize, level, state, timeLeft) {
        this.uid = uuid; //id của loai building - tên config - like CLC_1
        this.b_ID = BID; // id của building
        this.level = level;
        this.buildSize = buildSize;
        this.stateBuild = state;
        this.finishTime = timeLeft;

        //Add nền
        var strFileSubStrate = "#" + this.buildSize +".png";
        this.buildingSubstrateBottom = new cc.Sprite(strFileSubStrate);
        this.addChild(this.buildingSubstrateBottom, zorder.z_substrate);

        //add shadow
        var strFileShadow;
        if (uuid === "DEF_1" || uuid === "DEF_2") {
            var idleLevel = this.level;

            if (uuid === "DEF_2" && this.level > 6)
                idleLevel = 6;
            strFileShadow = "#" + uuid + "_" +  idleLevel + "_Shadow.png";
        }
        else
            strFileShadow = "#GRASS_"+ this.buildSize +"_Shadow.png";

        this.shadow = cc.Sprite(strFileShadow);
        this.addChild(this.shadow, zorder.z_shadow);

        this.drawAnim();
        this.drawNameLevel();
        this.drawArrow();
        this.setRedSubtrateFace();
        this.setGreenSubtrateFace();
        this.isShowName(false);
        this.isShowSubtrateArrow(false);
        this.isShowSubtrateFace(false);

        if (this.stateBuild === BUILDING_STATUS.BUILDING_BUILDING || this.stateBuild === BUILDING_STATUS.BUILDING_CREATE) {
            this.upgradeBuilding(this.finishTime);
        }

        if (this.b_ID === MAXBUILDINGID)
            this.addCancelOrAdd();
    },

    drawAnim: function() {
        var animMng = new AnimManager();
        var anim_state = animMng.judgeAnimType(this);
        this.set_ani_state(anim_state);
        this.addChild(this.ani_state, zorder.z_shadow);
    },

    upgradeEffect: function() {
        // Animation
        cc.spriteFrameCache.addSpriteFrames("res/Arts/Effects/levelup/anim.plist");

        var plist = cc.loader.getRes("res/Arts/Effects/levelup/anim.plist");
        var animFrames = [];
        var str = "";
        var frame;
        for (var i = 0; i < 12; i++) {
            if (i < 10)  str = "0" + i+ ".png";
            else str = i + ".png";
            frame = cc.spriteFrameCache.getSpriteFrame(str);
            animFrames.push(frame);
        }
        var animation =  cc.Animation(animFrames, 0.1);
        this._upgrade =  cc.Sprite();

        this.addChild(this._upgrade, 5);
        this._upgrade.runAction( cc.sequence( cc.animate(animation), cc.callFunc(this.destroy, this) ) );

        cc.spriteFrameCache.removeSpriteFramesFromFile("res/Arts/Effects/levelup/anim.plist");

    },

    destroy:function () {
        this._upgrade.stopAllActions();
        this.removeChild(this._upgrade);

    },

    drawNameLevel: function() {
        this.LabelName = new cc.LabelBMFont(gv.buildInfo[this.uid].name, "res/Fonts/soji_24.fnt");
        this.LabelName.color = new cc.Color(255, 255, 0);
        this.LabelName.setAnchorPoint(0.5, 0.5);
        this.LabelName.y = this.buildSize/2 * BUILDINGBASEGRID_HEIGHT;
        this.addChild(this.LabelName, zorder.z_labelName);

        this.labelLevel = new cc.LabelBMFont("Cấp " + this.level, "res/Fonts/soji_20.fnt");
        this.labelLevel.setAnchorPoint(0.5, 0.5);
        this.labelLevel.y = this.LabelName.y - this.LabelName.height;
        this.addChild(this.labelLevel, zorder.z_labelName);
    },

    isShowName: function(bool) {
        this.LabelName.setVisible(bool);
        this.labelLevel.setVisible(bool);
    },

    isShowClickInfo: function(bool) {
        try {
            cc.audioEngine.runEffect("res/Sounds/sfx/" + this.uid + "_pickup.mp3");
        }
        catch (e) {
            //do nothing;
        }
        this.isShowSubtrateArrow(bool);
        this.isShowName(bool);
        this.ani_state.runEffect(bool);
    },

    drawArrow: function() {
        var strFile = "#arrowmove" + this.buildSize + ".png";
        this.arrowSprite = cc.Sprite(strFile);
        this.arrowSprite.scale = 0;
        this.addChild(this.arrowSprite, zorder.z_arrow);
    },

    isShowSubtrateArrow: function(bool) {
        if (this.arrowSprite != null) {
            if (bool) {
                this.arrowSprite.runAction(cc.scaleTo(0.2, 1));
            }
            else {
                this.arrowSprite.runAction(cc.scaleTo(0.2, 0));
            }
        }
    },

    isShowSubtrateFace: function(bool) {
        if (bool) {
            if (this.showGreenSubstrate)
                this.greenSubtrate.setVisible(true);
            else
                this.redSubtrate.setVisible(true);
        }
        else {
            this.greenSubtrate.setVisible(false);
            this.redSubtrate.setVisible(false);
        }
    },

    setRedSubtrateFace: function() {
        var strFile = "#RED_" + this.buildSize + ".png";
        this.redSubtrate = cc.Sprite(strFile);
        this.redSubtrate.scale = SUBSTRATE_SCALE;
        this.addChild(this.redSubtrate, zorder.z_redSubstrate);
    },

    setGreenSubtrateFace: function() {
        var strFile = "#GREEN_" + this.buildSize + ".png";
        this.greenSubtrate = cc.Sprite(strFile);
        this.greenSubtrate.scale = SUBSTRATE_SCALE;
        this.addChild(this.greenSubtrate, zorder.z_greenSubstrate);
    },

    setMapIndex: function(isoAt) {
        this.mapIndex = isoAt;
    },

    upgradeBuilding: function(timeLeft) {
        if (timeLeft <= 0) {
            this.stateBuild = BUILDING_STATUS.BUILDING_FREE;
            return;
        }
        if (this.stateBuild === BUILDING_STATUS.BUILDING_BUILDING)
            var level = this.level + 1;
        else level = this.level;

        var baseBuildNextLevel = baseGameBuildingInfo[this.uid][level];
        this.timeBuild = baseBuildNextLevel["buildTime"];
        this.count = this.timeBuild - timeLeft;

        if (this.count < this.timeBuild) {
            var strTime = Util.calculate_d_h_m_s(this.timeBuild - this.count);
            var percent = (1 - (this.timeBuild - this.count)/this.timeBuild ) * 100;

            //fix ui label
            this.LabelName.y += 75;
            this.labelLevel.y += 75;
            this.ani_state.drawUpgrade(this.stateBuild, strTime, percent);

            //Add time and slide to schedule
            this.schedule(this.updateBuildStatus, 1);
        }
    },

    updateBuildStatus: function(dt) {
        this.count += 1;
        this.ani_state.trainBar.setPercent((this.count / this.timeBuild) * 100 );
        var strTime = Util.calculate_d_h_m_s(this.timeBuild - this.count);
        this.ani_state.timeLabel.setString(strTime);

        if (this.count >= this.timeBuild) {
            //SERVER: Upgrade building xong
            testnetwork.connector.sendFinishMapAction(this.b_ID); // Building lên cấp mới
            if (this.stateBuild === BUILDING_STATUS.BUILDING_BUILDING)
                this.drawLevel(this.level + 1);
            else if (this.stateBuild === BUILDING_STATUS.BUILDING_CREATE) {
                this.drawLevel(this.level);
            }
        }
    },

    removeBuilding: function() {
        this.unschedule(this.updateBuildStatus);

        //Xóa building khi xây nhà mấy xong cancel
        var centralLayer = gv.centralScene.getChildByName("screen");
        if (centralLayer.m_building) {
            if (centralLayer.m_building.b_ID === this.b_ID && !centralLayer.m_BuyBuilding)
                gv.centralScene.UILayer.InfoBtnDisappear(centralLayer.m_building);
        }

        gv.centralScene.getChildByName("screen").RemoveGridBuildInfo(this.mapIndex, this.buildSize);
        gv.centralScene.getChildByName("screen").m_building = null;
        this.ani_state.runEffect(false);
        this.set_ani_state(null);
        this.removeAllChildrenWithCleanup(true);
    },

    drawLevel: function(level) {
        cc.audioEngine.playEffect( gameRes.building_finish);

        if (level === -1)
            return;

        if (level === 0) {
            this.removeBuilding();
            return;
        }

        this.unschedule(this.updateBuildStatus);

        var centralUI = cc.director.getRunningScene().getChildByName("UILayer");
        var centralLayer = cc.director.getRunningScene().getChildByName("screen");

        if (centralLayer.m_building) {
            if (centralLayer.m_building.b_ID === this.b_ID) {
                centralUI.InfoBtnDisappear(centralLayer.m_building);
                this.isShowClickInfo(true);
            }
        }

        //reset Anim
        this.level = level;
        this.stateBuild = BUILDING_STATUS.BUILDING_FREE;
        this.upgradeEffect();
        this.set_ani_state(null);
        this.drawAnim();

        //Reset level
        this.removeChild(this.LabelName);
        this.removeChild(this.labelLevel);
        this.drawNameLevel();
        this.isShowClickInfo(false);

        if (centralLayer.m_building) {
            if (centralLayer.m_building.b_ID === this.b_ID) {
                centralUI.InfoBtnAppear(this);
            }
        }

        //upgrade gv level
        if (this.uid === "TOW_1")
            gv.townHallLevel = this.level;

    },

    addCancelOrAdd: function() {
        this.stateBuild = BUILDING_STATUS.BUILDING_CREATE;

        var cancel = new ccui.Button("res/Arts/GUIs/cancel.png");
        cancel.setName("cancelBtn");
        cancel.setPosition(this.buildingSubstrateBottom.x - this.buildingSubstrateBottom.width/2, this.buildingSubstrateBottom.y + this.buildingSubstrateBottom.height/2);
        cancel.addTouchEventListener(this.cancelBuy_touchEvent, this);
        this.addChild(cancel, 100);

        var add = new ccui.Button("res/Arts/GUIs/accept.png");
        add.setName("addBtn");
        add.setPosition(this.buildingSubstrateBottom.x + this.buildingSubstrateBottom.width/2, this.buildingSubstrateBottom.y + this.buildingSubstrateBottom.height/2);
        add.addTouchEventListener(this.accBuy_touchEvent, this);
        this.addChild(add, 100);

        this.isShowSubtrateArrow(true);
        this.isShowName(false);
        this.ani_state.runEffect(true);
        this.isShowSubtrateFace(true);
        this.setLocalZOrder(MAX_ZORDER);

        var centralLayer = gv.centralScene.getChildByName("screen");
        centralLayer.m_BuyBuilding = this;
        centralLayer.m_building = this;
        centralLayer.buildNewMapIndex = this.mapIndex;
        centralLayer.m_BuildOperatingMode = OPERATINGMODE.MODE_DRAG;
    },

    cancelBuy_touchEvent: function(sender, type) {
        switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                //Call remove building
                this.removeBuilding();
                var centralLayer = gv.centralScene.getChildByName("screen");

                centralLayer.m_BuyBuilding = null;
                centralLayer.m_building = null;
                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    accBuy_touchEvent: function (sender, type) {
        switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                //Call add building

                gv.acceptBuy = true;

                var centralLayer = gv.centralScene.getChildByName("screen");
                var baseInfo = this.getCostRequired();

                for (var i = 0; i < gv.upgradeCost.length; i++) {
                    var upgradeType = gv.upgradeType[i];
                    if (baseInfo[upgradeType] > gv.userData[upgradeType]) {
                        gv.acceptBuy = false;
                        gv.centralScene.showNoti("lackLobby", 0);
                    }
                }

                if (this.uid !== "BDH_1" && gv.userData[LOBBY.BUILDER] <= 0) {
                    gv.centralScene.showNoti("maxBuilder", 0);
                    gv.acceptBuy = false;
                }

                if (gv.acceptBuy) {
                    testnetwork.connector.sendMapCreateItem(this.uid, centralLayer.buildNewMapIndex.x, centralLayer.buildNewMapIndex.y);
                    centralLayer.m_BuyBuilding = null;
                    centralLayer.m_building = null;
                    this.removeChildByName("cancelBtn");
                    this.removeChildByName("addBtn");
                    this.removeBuilding();
                }

                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    getCostRequired: function () {
        var buildType = this.uid;
        var buildLevel = this.level + 1;
        if (this.stateBuild === BUILDING_STATUS.BUILDING_CREATE)
            buildLevel -= 1;
        var baseBuildInfo = baseGameBuildingInfo[buildType][buildLevel];

        var lobbyType;
        gv.upgradeCost = [];
        gv.upgradeType = [];

        if (baseBuildInfo.elixir !== undefined && baseBuildInfo.elixir !== 0) {
            lobbyType = "elixir";
            gv.upgradeCost.push(baseBuildInfo[lobbyType]);
            gv.upgradeType.push(lobbyType);
        }
        if (baseBuildInfo.gold !== undefined && baseBuildInfo.gold !== 0) {

            lobbyType = "gold";
            gv.upgradeCost.push(baseBuildInfo[lobbyType]);
            gv.upgradeType.push(lobbyType);
        }
        if (baseBuildInfo.darkElixir !== undefined && baseBuildInfo.darkElixir !== 0) {

            lobbyType = "darkElixir";
            gv.upgradeCost.push(baseBuildInfo[lobbyType]);
            gv.upgradeType.push(lobbyType);
        }

        return baseBuildInfo;
    }
});