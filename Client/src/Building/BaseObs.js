/**
 * Created by CPU02302_LOCAL on 12/7/2018.
 */

var BaseObs = Unit.extend({

    buildingSubstrateBottom: null,
    arrowSprite: null,
    buildSize: null,
    mapIndex: null,
    uid: null,
    b_ID: null,

    ctor: function() {
        this._super();
    },

    init: function(uuid, BID, buildSize) {

        this.uid = uuid; //id của building
        this.b_ID = BID; // id của loại building
        this.level = 0;
        this.stateBuild = BUILDING_STATUS.BUILDING_FREE;

        this.buildSize = buildSize;

        //Add nền
        var strFileSubStrate = "#GRASS_0_" + this.buildSize + "_OBS.png";
        this.buildingSubstrateBottom = cc.Sprite(strFileSubStrate);
        this.addChild(this.buildingSubstrateBottom, zorder.z_substrate);

        this.drawArrow();
        this.isShowSubtrateArrow(false);

        //Add Build nên trên nền
        //load idle

         var strFile = resIdle.OBS + this.uid + "/idle/image0000.png";
        this.idle = cc.Sprite(strFile);
        this.addChild(this.idle, zorder.z_idle);

        this.drawNameLevel();
        this.isShowName(false);

    },

    drawArrow: function() {
        var strFile = "#arrowmove" + this.buildSize + ".png";
        this.arrowSprite = cc.Sprite(strFile);
        this.arrowSprite.scale = 0;
        this.addChild(this.arrowSprite, zorder.z_arrow);
    },

    isShowSubtrateArrow: function(bool) {
        if (this.arrowSprite != null) {
            if (bool) {
                this.arrowSprite.runAction(cc.scaleTo(0.2, 1));
            }
            else {
                this.arrowSprite.runAction(cc.scaleTo(0.2, 0));
            }
        }
    },

    drawNameLevel: function() {
        this.LabelName = new cc.LabelBMFont(gv.ObsInfo[this.uid].name, "res/Fonts/soji_24.fnt");
        this.LabelName.color = new cc.Color(255, 255, 0);
        this.LabelName.setAnchorPoint(0.5, 0.5);
        this.LabelName.y = this.buildSize/2 * BUILDINGBASEGRID_HEIGHT;
        this.LabelName.setName("labelName");
        this.addChild(this.LabelName, zorder.z_labelName);
    },

    isShowName: function(bool) {
        this.LabelName.setVisible(bool);
    },

    isShowClickInfo: function(bool) {
        this.isShowSubtrateArrow(bool);
        this.isShowName(bool);
        this.runEffect(bool);
    },

    runEffect: function(bool) {
        var seqRep = cc.sequence(cc.TintTo(0.5, 202, 204, 206), cc.TintTo(0.5, 255, 255, 255)).repeatForever();
        if (bool) {
            var seq = cc.sequence(cc.scaleTo(0.2, 1.2), cc.scaleTo(0.2, 1));
            this.idle.runAction(seq);
            this.idle.runAction(seqRep);
        }
        else {
            this.idle.color = new cc.Color(255, 255, 255);
            this.idle.stopAllActions();
        }
    },

    setMapIndex: function(isoAt) {
        this.mapIndex = isoAt;
    },

    removeObs: function(timeLeft) {

        var baseBuildNextLevel = baseGameBuildingInfo[this.uid][this.level + 1];
        this.timeBuild = baseBuildNextLevel["buildTime"];
        this.stateBuild = BUILDING_STATUS.BUILDING_BUILDING;
        this.count = this.timeBuild - timeLeft;

        if (this.count < this.timeBuild) {
            var strTime = Util.calculate_d_h_m_s(this.timeBuild - this.count);
            var percent = (this.count/this.timeBuild ) * 100;
            //fix ui label
            this.LabelName.y += 75;
            this.drawUpgrade(strTime, percent);
            //Add time and slide to schedule
            this.schedule(this.updateRemoveStatus, 1);
        }
    },

    updateRemoveStatus: function(dt) {
        this.count += 1;

        this.trainBar.setPercent( (this.count/this.timeBuild ) * 100 );
        var strTime = Util.calculate_d_h_m_s(this.timeBuild - this.count);
        this.timeLabel.setString(strTime);

        if (this.count >= this.timeBuild) {
            testnetwork.connector.sendFinishMapAction(this.b_ID); // Building lên cấp mới
            this.unschedule(this.updateRemoveStatus);
            this.removeFinish();
        }

    },

    removeFinish: function() {
        this.LabelName.y -= 75;
        var centralLayer = gv.centralScene.getChildByName("screen");
        centralLayer.RemoveGridBuildInfo(this.mapIndex, this.buildSize);
        if (centralLayer.m_building) {
            if (centralLayer.m_building.b_ID === this.b_ID) {
                gv.centralScene.UILayer.InfoBtnDisappear(centralLayer.m_building);
                centralLayer.m_building = null;
            }
        }
        else
            centralLayer.m_building = null;

        this.setVisible(false);
    },

    drawUpgrade: function( timeBuild, percent) {

            //Add build slide
            //bg train bar
            cc.spriteFrameCache.addSpriteFrames(res.TrainTroopUI_plist);
            var loadingBar = cc.Sprite("#bg_train_bar.png");
            loadingBar.scale = 2;
            loadingBar.y += this.buildSize * BUILDINGBASEGRID_HEIGHT/2 - loadingBar.height/2;
            this.addChild(loadingBar, zorder.z_anim+1);

            //add train bar
            this.trainBar = new ccui.LoadingBar();
            this.trainBar.setName("TrainBar");
            this.trainBar.scale = 2;
            this.trainBar.loadTexture("train_bar.png", ccui.Widget.PLIST_TEXTURE);
            this.trainBar.setAnchorPoint(0, 0.5);
            this.trainBar.x = loadingBar.x - loadingBar.width/2 * loadingBar.scale;
            this.trainBar.y = loadingBar.y;
            this.trainBar.setPercent(percent);
            this.addChild(this.trainBar, zorder.z_anim + 2);

            //add Time build
            this.timeLabel = new cc.LabelBMFont(timeBuild, "res/Fonts/soji_20.fnt");
            this.timeLabel.y = this.trainBar.y + this.trainBar.height/2 + this.timeLabel.height/2;
            this.addChild(this.timeLabel, zorder.z_anim + 2);

            cc.spriteFrameCache.removeSpriteFramesFromFile(res.TrainTroopUI_plist);

    },

    getCostRequired: function () {
        var buildType = this.uid;
        var baseBuildInfo = baseGameBuildingInfo[buildType][1];

        var lobbyType;
        gv.upgradeCost = [];
        gv.upgradeType = [];

        if (baseBuildInfo.elixir !== undefined && baseBuildInfo.elixir !== 0) {
            lobbyType = "elixir";
            gv.upgradeCost.push(baseBuildInfo[lobbyType]);
            gv.upgradeType.push(lobbyType);
        }
        if (baseBuildInfo.gold !== undefined && baseBuildInfo.gold !== 0) {

            lobbyType = "gold";
            gv.upgradeCost.push(baseBuildInfo[lobbyType]);
            gv.upgradeType.push(lobbyType);
        }
        if (baseBuildInfo.darkElixir !== undefined && baseBuildInfo.darkElixir !== 0) {

            lobbyType = "darkElixir";
            gv.upgradeCost.push(baseBuildInfo[lobbyType]);
            gv.upgradeType.push(lobbyType);
        }

        return baseBuildInfo;
    }

});