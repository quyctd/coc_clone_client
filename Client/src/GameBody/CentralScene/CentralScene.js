/**
 * Created by CPU02302_LOCAL on 12/4/2018.
 */

// Get Central Layer

var CentralScene = cc.Scene.extend({
    mapLayer: null,
    scaleSize: 1.5,

    ctor: function() {
        this._super();
        this.create();
    },

    create: function() {
        var Layer = new CentralLayer();
        Layer.setName("screen");
        this.addChild(Layer, 1);


        var lobbyLayer = new LobbyLayer();
        lobbyLayer.setName("lobby");
        this.addChild(lobbyLayer, 2);

        this.UILayer = new CentralUILayer();
        this.UILayer.setName("UILayer");
        this.addChild(this.UILayer, 3);

        this.bgColor = cc.LayerColor(cc.color(51, 51, 51, 0.3 * 255));
        this.addChild(this.bgColor, 4);
        this.bgColor.setVisible(false);

        this.noti = new Notification();
        this.noti.init("cancel");
        this.addChild(this.noti, 5);
        this.noti.setVisible(false);

        this.dialog = new Dialog();
        this.addChild(this.dialog, 5);
        this.dialog.setVisible(false);

        this._errorMsg = cc.LabelBMFont("", "res/Fonts/soji_24.fnt");
        this._errorMsg.setColor(cc.color.RED);
        this.addChild(this._errorMsg, 1000);

        this._successMsg = cc.LabelBMFont("", "res/Fonts/soji_24.fnt");
        this._successMsg.setColor(cc.color.GREEN);
        this.addChild(this._successMsg, 1000);

        if (gv.sound) {
            cc.audioEngine.setMusicVolume(0.5);
            cc.audioEngine.playMusic(gameRes.bgMusic_mp3, true);
        }

        return this;
    },

    pauseLayer: function() {
        this.bgColor.setVisible(true);

        cc.eventManager.pauseTarget(this.getChildByName("lobby"), true);
        cc.eventManager.pauseTarget(this.getChildByName("screen"), true);
        cc.eventManager.pauseTarget(this.getChildByName("UILayer"), true);
    },

    resumeLayer: function() {
        this.bgColor.setVisible(false);

        cc.eventManager.resumeTarget(this.getChildByName("lobby"), true);
        cc.eventManager.resumeTarget(this.getChildByName("screen"), true);
        cc.eventManager.resumeTarget(this.getChildByName("UILayer"), true);
    },

    showNoti: function(type, value) {
        this.pauseLayer();
        this.noti.setVisible(true);
        this.noti.changeContentByActionType(type, value);
        this.noti.scale = 0.2;
        var seqScale = cc.sequence(cc.scaleTo(0.25, 1));
        this.noti.runAction(seqScale);
    },

    showDialog: function (building, type) {
        this.pauseLayer();
        this.dialog.setVisible(true);
        this.dialog.changeContentByBuilding(building, type);
        this.dialog.scale = 0.2;

        var seqScale = cc.sequence(cc.scaleTo(0.2, 1));
        this.dialog.runAction(seqScale);
    },


    showError: function(str) {
        var winSize = cc.director.getWinSize();

        gv.centralScene._errorMsg.setString(str);
        gv.centralScene._errorMsg.setPosition(winSize.width/2, winSize.height/2);
        var fadeIn = cc.FadeIn(1);
        var fadeOut = cc.FadeOut(1);
        gv.centralScene._errorMsg.runAction(cc.Sequence.create(fadeIn, fadeOut));
    },

    showSucess: function(str) {
        var winSize = cc.director.getWinSize();

        gv.centralScene._successMsg.setString(str);
        gv.centralScene._successMsg.setPosition(winSize.width/2, winSize.height/2);
        var fadeIn = cc.FadeIn(1);
        var fadeOut = cc.FadeOut(1);
        gv.centralScene._successMsg.runAction(cc.Sequence.create(fadeIn, fadeOut));
    }


});
