CentralLayer.prototype.buildTouchesBegan = function(touches, event) {
    this.m_enHandleState = MAPHANDLESTATE.STATE_CLICK;
};

CentralLayer.prototype.buildTouchesMoved = function(touches, event) {

    var touch = touches[0];
    if (touch != null) {
        var touchLocation = touch.getLocationInView();
        touchLocation = cc.director.convertToGL(touchLocation);
        // Chuyển trạng thái sang drag
        if (this.m_BuildOperatingMode === OPERATINGMODE.MODE_DRAG && this.m_building != null && this.m_enHandleState === MAPHANDLESTATE.STATE_CLICK) {

            if (!this.isClickBuilding(touchLocation, this.m_building, this.buildNewMapIndex)) {
                this.m_enHandleState = MAPHANDLESTATE.STATE_DRAG;
            }
        }

        //MOVE khi click vào building ở map
        if (this.m_enHandleState === MAPHANDLESTATE.STATE_CLICK) {
                    this.m_enHandleState = MAPHANDLESTATE.STATE_MOVE;
        }

        if (this.m_OperatingMode === OPERATINGMODE.MODE_NULL) {
            if (this.m_BuildOperatingMode === OPERATINGMODE.MODE_DRAG && this.m_building != null && this.m_enHandleState === MAPHANDLESTATE.STATE_MOVE) {

                var pointMap = tmxTiledMap.convertToNodeSpace( touchLocation );
                var row = Math.floor((MAPHEIGHTMAX-pointMap.y)/BUILDINGBASEGRID_HEIGHT);
                var col = Math.floor((pointMap.x-BUILDINGBASEGRID_WIDTH/2)/BUILDINGBASEGRID_WIDTH) + 1;
                var info = this.getTouchRectInfo(row*MAPGRIDWIDTH+col);
                if (info != null) {
                    var newIndex = this.touchInGrid(info.mapIndex, pointMap);
                    this.buildNewMapIndex = newIndex;
                    if (newIndex) {
                        var xy = this.positionForIsoAt(newIndex);
                        xy.x = xy.x + BUILDINGBASEGRID_WIDTH/2;
                        xy.y = xy.y - this.m_building.buildSize/2 * BUILDINGBASEGRID_HEIGHT;
                        this.m_building.setPosition(xy);
                    }
                    // cc.log(newIndex.x.toString(), newIndex.y.toString());
                    if (this.isCanPutDownBuild(this.buildNewMapIndex, this.m_building.buildSize, this.m_building.b_ID))
                        this.m_building.showGreenSubstrate = true;

                    else this.m_building.showGreenSubstrate = false;

                    this.m_building.isShowSubtrateFace(false);
                    this.m_building.isShowSubtrateFace(true);
                }

            }
        }
        else if (this.m_OperatingMode === OPERATINGMODE.MODE_DRAG) {
        }
    }

};

CentralLayer.prototype.buildTouchesEnded = function(touches, event) {

    var touch = touches[0];
    if (touch != null)
    {
        var touchLocation = touch.getLocationInView();
        touchLocation = cc.director.convertToGL(touchLocation);

        if (this.m_OperatingMode === OPERATINGMODE.MODE_NULL) {
            if(this.m_BuyBuilding != null) return;

            //Slected building
            if (this.m_enHandleState === MAPHANDLESTATE.STATE_CLICK && this.m_building != null) {

                //is Can put down
                if (this.isCanPutDownBuild(this.buildNewMapIndex, this.m_building.buildSize, this.m_building.b_ID) && this.m_building.b_ID > MAXOBSID)
                {
                    if (this.buildNewMapIndex !== this.m_building.mapIndex) {
                        testnetwork.connector.sendMapMoveItem(this.m_building.b_ID, this.buildNewMapIndex.x, this.buildNewMapIndex.y);
                        this.RemoveGridBuildInfo(this.m_building.mapIndex, this.m_building.buildSize);
                        this.m_building.mapIndex = this.buildNewMapIndex;
                        this.MarkGridBuildInfo(this.m_building.mapIndex, this.m_building.buildSize, this.m_building.b_ID, this.m_building);

                    }
                }
                else
                {
                    var xy = this.positionForIsoAt(this.m_building.mapIndex);
                    xy.x = xy.x + BUILDINGBASEGRID_WIDTH/2;
                    xy.y = xy.y - this.m_building.buildSize/2 * BUILDINGBASEGRID_HEIGHT;

                    this.m_building.setPosition(xy);
                    //bỏ hiện mũi tên

                    if (this.m_building.b_ID > MAXOBSID) {
                        this.m_building.isShowSubtrateFace(false);
                        this.m_building.showGreenSubstrate = true;
                    }
                }
                this.m_building.isShowClickInfo(false);
                gv.centralScene.UILayer.InfoBtnDisappear(this.m_building);

                this.setZOrderByIso(this.m_building);
                this.m_BuildOperatingMode = OPERATINGMODE.MODE_NULL;
                this.m_building = null;
            }

            //Chưa có tòa nhà nào được chọn
            if (this.m_enHandleState === MAPHANDLESTATE.STATE_CLICK && this.m_building === null)
            {
                var pBuilding = this.SelectBuildingFromSubstrate(touchLocation);

                if (pBuilding == null) {
                    this.m_building = null;
                }
                else {
                    this.m_building = pBuilding;
                    this.buildNewMapIndex = this.m_building.mapIndex;

                    //Hien thi mui ten
                    this.m_building.isShowClickInfo(true);
                    this.m_building.setLocalZOrder(MAX_ZORDER);
                    if (this.m_building.b_ID > MAXOBSID) {
                        this.m_BuildOperatingMode = OPERATINGMODE.MODE_DRAG;
                    }

                    gv.centralScene.UILayer.InfoBtnAppear(this.m_building);
                }
            }

        }
        else if (this.m_OperatingMode === OPERATINGMODE.MODE_DRAG)
        {
        }
        this.m_enHandleState = MAPHANDLESTATE.STATE_NULL;
    }
};

CentralLayer.prototype.putBuildingDown = function(id, posx, posy) {

    var newPos = cc.p(posx, posy);
    var building = this.getBuildingById(id);
    if (building.mapIndex === newPos)
        return ;

    else {
        this.RemoveGridBuildInfo(building.mapIndex, building.buildSize);

        building.mapIndex = newPos;

        var xy = this.positionForIsoAt(building.mapIndex);
        xy.x = xy.x + BUILDINGBASEGRID_WIDTH/2;
        xy.y = xy.y - building.buildSize/2 * BUILDINGBASEGRID_HEIGHT;

        building.setPosition(xy);

        this.MarkGridBuildInfo(newPos, building.size, id, building);

        if (id > MAXOBSID)
            building.isShowSubtrateFace(false);
    }

};