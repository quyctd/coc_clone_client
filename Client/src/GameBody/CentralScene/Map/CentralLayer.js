/**
 * Created by CPU02302_LOCAL on 12/4/2018.
 */

var CentralLayer = cc.Layer.extend({

    unit_list_:null,
    nScale: 0.5,
    tmxTiledMap: null,
    tileSize: null,
    mapSize: null,
    listMapGridLogicInfo: {},
    listTouchRectInfo: {},
    build_uuId: 0,
    m_building: null,
    m_BuyBuilding: null,
    m_OperatingMode: OPERATINGMODE.MODE_NULL,
    m_BuildOperatingMode: null,
    m_enHandleState: MAPHANDLESTATE.STATE_NULL,
    buildNewMapIndex: null,
    startMovePos: null,
    initialDistance: 0, // Map zoom: khoảng cách ban đầu giữa 2 điểm
    finalDistance: 0, // Map zoom: Khoảng cách cuối cùng giữa 2 điểm
    currentScale: 1, // Lần sau vào còn biết

    ctor:function () {
        this._super();
        this.init();
    },

    init: function(){

        //load building info config
        cc.loader.loadJson(res.buildInfoData, function(err, ret) {
            gv.buildInfo = ret;
        });
        cc.loader.loadJson(res.obsInfoData, function(err, ret) {
            gv.ObsInfo = ret;
        });

        //load ui data
        cc.loader.loadJson(gameRes.uiBtnConfig, function(err, ret) {
            gv.uiBtnConfig = ret;
        });

        cc.loader.loadJson(gameRes.uiTranslater, function(err, ret) {
            gv.uiTranslater = ret;
        });

        //load Plist BG
        cc.spriteFrameCache.addSpriteFrames(res.mapLayer_plist);
        cc.spriteFrameCache.addSpriteFrames(res.mapGrass_plist);
        cc.spriteFrameCache.addSpriteFrames(res.mapObjBg_plist);
        cc.spriteFrameCache.addSpriteFrames(res.mapMoveUtil_plist);
        //Defense base
        cc.spriteFrameCache.addSpriteFrames(res.defenseBase_plist);
        //Animation Upgrade


        FileManager.loadBaseGameBuildingInfo();

        this.stateLoad();
        this.initMap();
        this.addTouchListener();
        this.loadInitGame();



        return true;
    },

    initMap: function() {
        var winSize = cc.director.getWinSize();

        //load tiled map
        tmxTiledMap = new cc.TMXTiledMap.create(res.map_tmx);
        tmxTiledMap.anchorX = 0.5;
        tmxTiledMap.anchorY = 0.5;
        tmxTiledMap.scale = 0.5;
        tmxTiledMap.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(tmxTiledMap, zorder.z_map);

    },

    initBackground: function() {

        var mapSize = tmxTiledMap.getContentSize();

        var bg1 = new cc.Sprite("#1_0000_Layer-3.png");
        bg1.anchorX = 1;
        bg1.anchorY = 0;
        bg1.setPosition(mapSize.width/2, mapSize.height/2);
        bg1.scale = MAPBGSCALE;
        tmxTiledMap.addChild(bg1, zorder.z_background);

        var bg2 = new cc.Sprite("#1_0001_Layer-1.png");
        bg2.anchorX = 1;
        bg2.anchorY = 1;
        bg2.setPosition(mapSize.width/2,mapSize.height/2);
        bg2.scale = MAPBGSCALE;
        tmxTiledMap.addChild(bg2, zorder.z_background);

        var bg3 = new cc.Sprite("#1_0002_Layer-4.png");
        bg3.anchorX = 0;
        bg3.anchorY = 0;
        bg3.setPosition(mapSize.width/2,mapSize.height/2);
        bg3.scale = MAPBGSCALE;
        tmxTiledMap.addChild(bg3, zorder.z_background);

        var bg4 = new cc.Sprite("#1_0003_Layer-2.png");
        bg4.anchorX = 0;
        bg4.anchorY = 1;
        bg4.setPosition(mapSize.width/2,mapSize.height/2);
        bg4.scale = MAPBGSCALE;
        tmxTiledMap.addChild(bg4, zorder.z_background);
    },

    loadInitGame: function() {
        this.initBackground();

        //load init file
        var initGameData = {};
        cc.loader.loadJson(res.initGameData, function(err, ret) {
            initGameData = ret;
        });


        if (gv.userMap && gv.userObs) {
            var mapObj = gv.userMap;
            var obsObj = gv.userObs;
        }
        else {
            mapObj = initGameData['map'];
            obsObj = initGameData['obs'];
        }
        //load Obstacle
        for (var obsID in obsObj) {
            var obstacle = obsObj[obsID];

            this.addObs( obsID, obstacle);
        }

        //load building
        gv.lastBuildID = 0;
        for (var buildingID in mapObj) {
            var building = mapObj[buildingID];
            var key = buildingID;
            if(building.type === "TOW_1")
                gv.townHallLevel = building.level;
            gv.buildInfo[building.type].number += 1;
            this.addBuilding( key , building);
        }

    },

    stateLoad: function() {

        var i, j,info, M, N, row, col, index;
        //Initialize grid information
        for(  i = 0; i < MAPGRIDWIDTH; i++ )
        {
            for ( j=0; j<MAPGRIDHEIGHT; j++) {
                info = new MapGridLogicInfo();
                var id = Object.keys(this.listMapGridLogicInfo).length;
                info.uid = id;
                info.isBuilded = false;
                info.mapIndex = cc.p(i, j);
                index = i*MAPGRIDHEIGHT+j;
                this.AddMapGridLogicInfo(index, info);
            }
        }

        //Initialize touch rect information
        // upper triangle
        for ( i = 0; i < MAPGRIDWIDTH/2; i++) {
            for ( j = 0; j< 2*(i+1) -1; j++) {
                info = new TouchRectInfo();
                M = j;
                N = i*2 - j;
                info.mapIndex = cc.p(M, N);
                row = i;
                col = MAPGRIDWIDTH/2 - i + j;
                index = row*MAPGRIDWIDTH + col;
                this.AddTouchRectInfo(index, info);
            }
        }

        //lower triangle
        for ( i = MAPGRIDWIDTH/2; i< MAPGRIDWIDTH; i++) {
            for ( j = 0; j < (MAPGRIDWIDTH - 1 - i)*2 +1; j++) {
                info = new TouchRectInfo();
                M = (MAPGRIDWIDTH/2 - (MAPGRIDWIDTH -1 -i))*2 + j - 1;
                N = 42 - 1- j;
                info.mapIndex = cc.p(M, N);
                row = i;
                col = (MAPGRIDWIDTH/2-(MAPGRIDWIDTH-1-i))+j;
                index = row*MAPGRIDWIDTH + col;
                this.AddTouchRectInfo(index, info);
            }
        }

    },

    addTouchListener: function() {

        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ALL_AT_ONCE,

            onTouchesBegan: function(touches, event) {
                self.mapTouchesBegan(touches, event);
                self.buildTouchesBegan(touches, event);
            },

            onTouchesMoved: function(touches, event) {
                if (self.m_building === null || self.m_enHandleState === MAPHANDLESTATE.STATE_DRAG)
                    self.mapTouchesMoved(touches, event);
                self.buildTouchesMoved(touches, event);
            },

            onTouchesEnded: function(touches, event) {
                self.mapTouchesEnded(touches, event);
                self.buildTouchesEnded(touches, event);
            }
        }, this);
    },


    onDisconnect: function () {
        gv.centralScene.showNoti(NOTI_STATUS.DISCONNECT, 0);
    }

});
