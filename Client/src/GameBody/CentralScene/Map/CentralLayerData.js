/**
 * Created by CPU02302_LOCAL on 12/11/2018.
 */

function  MapGridLogicInfo() {
    this.b_ID = null;
    this.mapIndex = null;
    this.isBuilded = null;
    this.pBuilding = null; //point building
    this.size = null;
}

function TouchRectInfo() {
    this.mapIndex = null;

}

var OPERATINGMODE = {
    MODE_NULL : 0,
    MODE_DRAG : 1

};

var MAPHANDLESTATE = {
    STATE_NULL : 0, //Không làm gì
    STATE_ZOOM : 1,     //Zoom
    STATE_MOVE : 2,     //move
    STATE_CLICK : 3,    //click
    STATE_DRAG : 4     //drag
};