/**
 * Created by CPU02302_LOCAL on 12/12/2018.
 */

CentralLayer.prototype.SelectBuildingFromSubstrate = function (point) {
    var pbuilding = null;

    var ppoint = this.convertToNodeSpace(point); // Điểm chạm trên game
    var mappoint = tmxTiledMap.getPosition(); // Diem chạm trên bản đồ
    var dx = (ppoint.x - mappoint.x) * (ppoint.x - mappoint.x);
    var dy = (ppoint.y - mappoint.y) * (ppoint.y - mappoint.y);

    if (dx < MAPWIDTHMAX / 2 * MAPWIDTHMAX / 2 && dy < MAPHEIGHTMAX / 2 * MAPHEIGHTMAX / 2) {
        var pointMap = tmxTiledMap.convertToNodeSpace(point); // Convert thành điểm chạm trên bản đồ
        var row = Math.floor((MAPHEIGHTMAX - pointMap.y) / BUILDINGBASEGRID_HEIGHT);
        var col = Math.floor((pointMap.x - BUILDINGBASEGRID_WIDTH / 2) / BUILDINGBASEGRID_WIDTH) + 1;
        var info = this.getTouchRectInfo(row * MAPGRIDWIDTH + col);
        if (info != null) {
            var mapIndex = info.mapIndex;
            var newIndex;
            // Không xem xét 4 đỉnh
            if ((mapIndex.x === 0 && mapIndex.y === 0)
                || (mapIndex.x === 0 && mapIndex.y === MAPGRIDWIDTH - 1)
                || (mapIndex.x === MAPGRIDWIDTH - 1 && mapIndex.y === 0)
                || (mapIndex.x === MAPGRIDWIDTH - 1 && mapIndex.y === MAPGRIDWIDTH - 1)) {
                return pbuilding;
            }
            newIndex = this.touchInGrid(mapIndex, pointMap);
            if (!newIndex)
                return pbuilding;

            var gridIndex = newIndex.x * MAPGRIDHEIGHT + newIndex.y;
            info = this.getMapGridLogicInfo(gridIndex);
            if (info != null) {
                if (info.isBuilded) {
                    pbuilding = info.pBuilding;

                    return pbuilding;
                }
            }
        }
    }
    return pbuilding;
};

CentralLayer.prototype.AddMapGridLogicInfo = function (index, data) {
    this.listMapGridLogicInfo[index] = data;
};

CentralLayer.prototype.AddTouchRectInfo = function (index, data) {
    this.listTouchRectInfo[index] = data;
};

CentralLayer.prototype.getTouchRectInfo = function (index) {

    if (index in this.listTouchRectInfo) {
        var info = this.listTouchRectInfo[index];
        return info;
    }
    return null;
};

CentralLayer.prototype.getMapGridLogicInfo = function (index) {
    if (index in this.listMapGridLogicInfo) {
        var info = this.listMapGridLogicInfo[index];
        return info;
    }
    return null;
};

CentralLayer.prototype.positionForIsoAt = function (pos) {
    var xy = cc.p(BUILDINGBASEGRID_WIDTH / 2 * (MAPGRIDWIDTH + pos.x - pos.y - 1), BUILDINGBASEGRID_HEIGHT / 2 * ((MAPGRIDHEIGHT * 2 - pos.x - pos.y)));
    return xy;
};

CentralLayer.prototype.touchInGrid = function (mapIndex, pointMap) {
    var newIndex;
    pointMap.x = pointMap.x - BUILDINGBASEGRID_WIDTH / 2;

    //Cạnh trái bên trên
    if (mapIndex.x <= 0) {
        var rbIndex = cc.p( 1, mapIndex.y);
        var xy = this.positionForIsoAt(mapIndex);
        var tr = [];
        tr[0] = cc.p(xy.x + BUILDINGBASEGRID_WIDTH / 2, xy.y - BUILDINGBASEGRID_HEIGHT / 2);
        tr[1] = cc.p(xy.x + BUILDINGBASEGRID_WIDTH / 2, xy.y - BUILDINGBASEGRID_HEIGHT);
        tr[2] = cc.p(xy.x, xy.y - BUILDINGBASEGRID_HEIGHT);
        if (Util.inside(tr, pointMap)) {
            return rbIndex;
        }
    }

    //Cạnh phải bên dưới
    else if (mapIndex.x >= MAPGRIDWIDTH - 1) {
        var ltIndex = cc.p(MAPGRIDWIDTH - 1, mapIndex.y);
        xy = this.positionForIsoAt(mapIndex);
        tr = [];
        tr[0] = cc.p(xy.x, xy.y);
        tr[1] = cc.p(xy.x + BUILDINGBASEGRID_WIDTH / 2, xy.y);
        tr[2] = cc.p(xy.x + BUILDINGBASEGRID_WIDTH / 2, xy.y - BUILDINGBASEGRID_HEIGHT / 2);
        if (Util.inside(tr, pointMap)) {
            return ltIndex;
        }
    }

    //Cạnh phải bên trên
    else if (mapIndex.y <= 0) {
        var lbIndex = cc.p(mapIndex.x, 1);
        var xy = this.positionForIsoAt(mapIndex);
        var tr = [];
        tr[0] = cc.p(xy.x, xy.y - BUILDINGBASEGRID_HEIGHT);
        tr[1] = cc.p(xy.x - BUILDINGBASEGRID_WIDTH / 2, xy.y - BUILDINGBASEGRID_HEIGHT / 2);
        tr[2] = cc.p(xy.x - BUILDINGBASEGRID_WIDTH / 2, xy.y - BUILDINGBASEGRID_HEIGHT);
        if (Util.inside(tr, pointMap)) {
            return lbIndex;
        }
    }
    // Cạnh trái bên dưới
    else if (mapIndex.y >= MAPGRIDHEIGHT - 1) {
        var rtIndex = cc.p(mapIndex.x, MAPGRIDHEIGHT - 1);
        var xy = this.positionForIsoAt(mapIndex);
        var tr = [];
        tr[0] = cc.p(xy.x, xy.y);
        tr[1] = cc.p(xy.x - BUILDINGBASEGRID_WIDTH / 2, xy.y);
        tr[2] = cc.p(xy.x - BUILDINGBASEGRID_WIDTH / 2, xy.y - BUILDINGBASEGRID_HEIGHT / 2);
        if (Util.inside(tr, pointMap)) {
            return rtIndex;
        }
    }

    else {
        //
        {
            var rbIndex = cc.p(mapIndex.x + 1, mapIndex.y);
            var xy = this.positionForIsoAt(mapIndex); // Góc dưới bên phải
            var tr = [];
            tr[0] = cc.p(xy.x + BUILDINGBASEGRID_WIDTH / 2, xy.y - BUILDINGBASEGRID_HEIGHT / 2);
            tr[1] = cc.p(xy.x + BUILDINGBASEGRID_WIDTH / 2, xy.y - BUILDINGBASEGRID_HEIGHT);
            tr[2] = cc.p(xy.x, xy.y - BUILDINGBASEGRID_HEIGHT);
            if (Util.inside(tr, pointMap)) {
                return rbIndex;
            }
        }
        //
        {
            var ltIndex = cc.p(mapIndex.x - 1, mapIndex.y);
            var xy = this.positionForIsoAt(mapIndex); // Góc trên bên trái
            var tr = [];
            tr[0] = cc.p(xy.x, xy.y);
            tr[1] = cc.p(xy.x - BUILDINGBASEGRID_WIDTH / 2, xy.y);
            tr[2] = cc.p(xy.x - BUILDINGBASEGRID_WIDTH / 2, xy.y - BUILDINGBASEGRID_HEIGHT / 2);
            if (Util.inside(tr, pointMap)) {
                return ltIndex;
            }
        }
        //
        {
            var lbIndex = cc.p(mapIndex.x, mapIndex.y + 1);
            var xy = this.positionForIsoAt(mapIndex); // Góc dưới bên trái
            var tr = [];
            tr[0] = cc.p(xy.x, xy.y - BUILDINGBASEGRID_HEIGHT);
            tr[1] = cc.p(xy.x - BUILDINGBASEGRID_WIDTH / 2, xy.y - BUILDINGBASEGRID_HEIGHT / 2);
            tr[2] = cc.p(xy.x - BUILDINGBASEGRID_WIDTH / 2, xy.y - BUILDINGBASEGRID_HEIGHT);
            if (Util.inside(tr, pointMap)) {
                return lbIndex;
            }
        }
        //
        {
            var rtIndex = cc.p(mapIndex.x, mapIndex.y - 1);
            var xy = this.positionForIsoAt(mapIndex); // Góc trên bên phải
            var tr = [];
            tr[0] = cc.p(xy.x, xy.y);
            tr[1] = cc.p(xy.x + BUILDINGBASEGRID_WIDTH / 2, xy.y);
            tr[2] = cc.p(xy.x + BUILDINGBASEGRID_WIDTH / 2, xy.y - BUILDINGBASEGRID_HEIGHT / 2);
            if (Util.inside(tr, pointMap)) {
                return rtIndex;
            }
        }

        newIndex = mapIndex;
    }

    return newIndex;
};

CentralLayer.prototype.getMapIndexByTouchPoint = function (touchPoint) {
    var newMapIndex;
    var pointMap = tmxTiledMap.convertToNodeSpace(touchPoint);
    var row = Math.floor((MAPHEIGHTMAX - pointMap.y) / BUILDINGBASEGRID_HEIGHT);
    var col = Math.floor((pointMap.x - BUILDINGBASEGRID_WIDTH / 2) / BUILDINGBASEGRID_WIDTH) + 1;
    var info = this.getTouchRectInfo(row * MAPGRIDWIDTH + col);
    if (info != null) {
        var mapIndex = info.mapIndex;
        // do nothing for 4 corner
        if ((mapIndex.x === 0 && mapIndex.y === 0)
            || (mapIndex.x === 0 && mapIndex.y === MAPGRIDWIDTH - 1)
            || (mapIndex.x === MAPGRIDWIDTH - 1 && mapIndex.y === 0)
            || (mapIndex.x === MAPGRIDWIDTH - 1 && mapIndex.y === MAPGRIDWIDTH - 1)) {
            return null;
        }
        else {
            newMapIndex = this.touchInGrid(mapIndex, pointMap);
        }
    }

    return newMapIndex;
};

CentralLayer.prototype.MarkGridBuildInfo = function (lbIndex, row, b_ID, building) {

    var startRow = lbIndex.x;
    var startCol = lbIndex.y;
    for (var i = 0; i < row; i++) {
        for (var j = 0; j < row; j++) {
            var index = (startRow + i) * MAPGRIDHEIGHT + (startCol + j);
            var info = this.getMapGridLogicInfo(index);
            if (info != null) {
                this.listMapGridLogicInfo[index].isBuilded = true;
                this.listMapGridLogicInfo[index].b_ID = b_ID;
                this.listMapGridLogicInfo[index].pBuilding = building;
                this.listMapGridLogicInfo[index].pBuilding.size = building.buildSize;
            }
        }
    }
};

CentralLayer.prototype.RemoveGridBuildInfo = function (lbIndex, row) {
    var startRow = lbIndex.x;
    var startCol = lbIndex.y;
    for (var i = 0; i < row; i++) {
        for (var j = 0; j < row; j++) {
            var index = (startRow + i) * MAPGRIDHEIGHT + (startCol + j);
            var info = this.getMapGridLogicInfo(index);
            if (info != null) {
                this.listMapGridLogicInfo[index].isBuilded = false;
                this.listMapGridLogicInfo[index].b_ID = null;
                this.listMapGridLogicInfo[index].pBuilding = null;
            }
        }
    }
};

CentralLayer.prototype.getBuildingById = function(id) {
    for (var i = 0; i < MAPGRIDHEIGHT; i++) {
        for (var j = 0; j < MAPGRIDWIDTH; j++) {
            var index = i * MAPGRIDHEIGHT + j;
            if (this.listMapGridLogicInfo[index].b_ID === id) {
                return this.listMapGridLogicInfo[index].pBuilding;
            }
        }
    }
};

CentralLayer.prototype.isCanPutDownBuild = function (lBIndex, row, BID) {
    var isBuild = true;
    if (!lBIndex)
        return false;
    var startRow = lBIndex.x;
    var startCol = lBIndex.y;
    if ((startRow + row) > MAPGRIDWIDTH || (startCol + row) > MAPGRIDHEIGHT)
        return false;
    for (var i = 0; i < row; i++) {
        for (var j = 0; j < row; j++) {
            var index = (startRow + i) * MAPGRIDHEIGHT + (startCol + j);
            var info = this.getMapGridLogicInfo(index);
            if (info.isBuilded) {
                if (info.pBuilding.b_ID !== BID) {
                    isBuild = false;
                    break;
                }
            }
        }
    }
    return isBuild;
};

CentralLayer.prototype.isClickBuilding = function (point, build, buildNewMapIndex) {

    var isClick = false;
    var mapIndex = this.getMapIndexByTouchPoint(point);
    if (mapIndex != null) {
        var l = build.buildSize;
        var r = build.buildSize;
        var mX = mapIndex.x;
        var mY = mapIndex.y;
        var bX = buildNewMapIndex.x;
        var bY = buildNewMapIndex.y;

        if (mX < (bX + l) && mX >= bX && mY < (bY + r) && mY >= bY) {
            isClick = true;
        }
    }

    return isClick;
};

CentralLayer.prototype.setZOrderByIso = function(building) {
    var isoAt = building.mapIndex;
    var buildSize = building.buildSize;
    var z_order;
    var pointMap = this.positionForIsoAt(cc.p(isoAt.x + Math.ceil(buildSize/2), isoAt.y + Math.ceil(buildSize/2)));
    var row = Math.floor((MAPHEIGHTMAX-pointMap.y)/BUILDINGBASEGRID_HEIGHT);
    var col = Math.floor((pointMap.x-BUILDINGBASEGRID_WIDTH/2)/BUILDINGBASEGRID_WIDTH) + 1;
    z_order = row*MAPGRIDWIDTH + col;
    building.setLocalZOrder(z_order);
};

CentralLayer.prototype.generatePosCanPutDownBuild = function (buildSize) {

    // for (var i = 1; i < MAPGRIDWIDTH - buildSize; i++) {
    //     for (var j = 1; j <  MAPGRIDWIDTH - buildSize; j++) {
    //
    //         if (this.isCanPutDownBuild(cc.p( i, j), buildSize, 0))
    //             return cc.p(i, j);
    //     }
    // }

    var mainPoint = cc.p(MAPGRIDWIDTH/2, MAPGRIDHEIGHT/2);
    for (var i = 1; i < MAPGRIDWIDTH/2; i++) {

        if (this.isCanPutDownBuild(cc.p( mainPoint.x, mainPoint.y + i), buildSize, TEMP_BUILDINGID))
            return cc.p( mainPoint.x, mainPoint.y + i);

        if (this.isCanPutDownBuild(cc.p( mainPoint.x, mainPoint.y - i), buildSize, TEMP_BUILDINGID))
            return cc.p( mainPoint.x, mainPoint.y - i);

        if (this.isCanPutDownBuild(cc.p( mainPoint.x + i, mainPoint.y + i), buildSize, TEMP_BUILDINGID))
            return cc.p( mainPoint.x + i, mainPoint.y + i);

        if (this.isCanPutDownBuild(cc.p( mainPoint.x + i, mainPoint.y), buildSize, TEMP_BUILDINGID))
            return cc.p( mainPoint.x + i, mainPoint.y);

        if (this.isCanPutDownBuild(cc.p( mainPoint.x + i, mainPoint.y - i), buildSize, TEMP_BUILDINGID))
            return cc.p( mainPoint.x + i, mainPoint.y - i);

        if (this.isCanPutDownBuild(cc.p( mainPoint.x - i, mainPoint.y - i), buildSize, TEMP_BUILDINGID))
            return cc.p( mainPoint.x - i, mainPoint.y - i);

        if (this.isCanPutDownBuild(cc.p( mainPoint.x - i, mainPoint.y), buildSize, TEMP_BUILDINGID))
            return cc.p( mainPoint.x - i, mainPoint.y);

        if (this.isCanPutDownBuild(cc.p( mainPoint.x - i, mainPoint.y + i), buildSize, TEMP_BUILDINGID))
            return cc.p( mainPoint.x - i, mainPoint.y + i);
    }

    return cc.p(0, 0);
};