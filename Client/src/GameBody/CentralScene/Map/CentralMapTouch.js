/**
 * Created by CPU02302_LOCAL on 12/12/2018.
 */

CentralLayer.prototype.mapTouchesBegan = function (touches, event) {

};

CentralLayer.prototype.mapTouchesMoved = function (touches, event) {
    var mapSize = cc.size(tmxTiledMap.getContentSize().width, tmxTiledMap.getContentSize().height);
    var screenSize = cc.director.getWinSize();
    var touchCount = touches.length;

    var tempWidth, tempWidth_Half, tempHeight, tempHeight_Half, result1, result2, maxSelfPos , minSelfPos;
    //Move
    if (touchCount === 1) {
        var touch = touches[0];
        this.initialDistance = 0;
        this.finalDistance = 0;

        var touchLocation = touch.getLocationInView();
        var prevLocation = touch.getPreviousLocationInView();

        touchLocation = cc.director.convertToGL(touchLocation);
        prevLocation = cc.director.convertToGL(prevLocation);

        var diff = cc.pSub(touchLocation,prevLocation);

        var currentPos = this.getPosition();

         tempWidth = Math.ceil(mapSize.width * this.getScale() * MAPSCALE);
         tempWidth_Half = Math.ceil(tempWidth/2);
         tempHeight = Math.ceil(mapSize.height*this.getScale()* MAPSCALE);
         tempHeight_Half = Math.ceil(tempHeight/2);

         result1 = tempWidth_Half-screenSize.width/2 - 20;
         result2 = tempHeight_Half-screenSize.height/2 - 20;

         maxSelfPos = cc.p(result1,result2);

        minSelfPos = cc.p(result1-(tempWidth-1600) - 20,result2-(tempHeight-900) - 20);

        if (diff.x >=0) {
            diff.x = (this.getPositionX()+diff.x)<=maxSelfPos.x?diff.x:(maxSelfPos.x-this.getPositionX());
        }
        else {
            diff.x = (this.getPositionX()+diff.x)>=minSelfPos.x?diff.x:(minSelfPos.x-this.getPositionX());
        }

        if (diff.y>=0) {
            diff.y = (this.getPositionY()+diff.y)<=maxSelfPos.y?diff.y:(maxSelfPos.y-this.getPositionY());
        }
        else {
            diff.y = (this.getPositionY()+diff.y)>=minSelfPos.y?diff.y:(minSelfPos.y-this.getPositionY());
        }

        currentPos.x = this.getPositionX()>=minSelfPos.x?currentPos.x:minSelfPos.x;
        currentPos.y = this.getPositionY()>=minSelfPos.y?currentPos.y:minSelfPos.y;
        currentPos.x = this.getPositionX()<=maxSelfPos.x?currentPos.x:maxSelfPos.x;
        currentPos.y = this.getPositionY()<=maxSelfPos.y?currentPos.y:maxSelfPos.y;

        this.setPosition(cc.pAdd(currentPos, diff));
    }

    //Zoom
    else if (touchCount === 2) {

        var touch1, touch2, touch1Location, touch2Location;
        touch1 = touches[0];
        touch2 = touches[1];

        touch1Location = touch1.getLocationInView();
        touch2Location = touch2.getLocationInView();
        if ( this.initialDistance === 0 ) {
            touch1 = touches[0];
            touch2 = touches[1];

            touch1Location = touch1.getLocationInView();
            touch2Location = touch2.getLocationInView();
            this.initialDistance = Math.sqrt((touch1Location.x-touch2Location.x)*(touch1Location.x-touch2Location.x)
                +(touch1Location.y-touch2Location.y)*(touch1Location.y-touch2Location.y));
        }

        touch1 = touches[0];
        touch2 = touches[1];

        touch1Location = touch1.getLocationInView();
        touch2Location = touch2.getLocationInView();

        this.finalDistance = Math.sqrt((touch1Location.x-touch2Location.x)*(touch1Location.x-touch2Location.x)+(touch1Location.y-touch2Location.y)*(touch1Location.y-touch2Location.y));

        var scaleGesture = this.getScale() + (this.finalDistance - this.initialDistance)*0.005;

        scaleGesture = scaleGesture<3.7?scaleGesture:3.7;

        scaleGesture = scaleGesture>1?scaleGesture:1;

        this.setScale(scaleGesture);

        var newPos=this.getPosition();
        tempWidth = mapSize.width*this.getScale() * MAPSCALE;
        tempWidth_Half = tempWidth/2;
        tempHeight = mapSize.height*this.getScale() * MAPSCALE;
        tempHeight_Half =tempHeight/2;

        result1 = tempWidth_Half-screenSize.width/2;
        result2 = tempHeight_Half-screenSize.height/2;

        maxSelfPos = cc.p(result1,result2);
        minSelfPos = cc.p(result1-(tempWidth-1600),result2-(tempHeight-900));

        newPos.x = this.getPositionX()>=minSelfPos.x?newPos.x:minSelfPos.x;
        newPos.y = this.getPositionY()>=minSelfPos.y?newPos.y:minSelfPos.y;
        newPos.x = this.getPositionX()<=maxSelfPos.x?newPos.x:maxSelfPos.x;
        newPos.y = this.getPositionY()<=maxSelfPos.y?newPos.y:maxSelfPos.y;


        this.setPosition(newPos);


        this.currentScale = scaleGesture;
        this.initialDistance = this.finalDistance;

    }

};

CentralLayer.prototype.mapTouchesEnded = function (touches, event) {
    this.initialDistance = 0;
    this.finalDistance = 0;

};