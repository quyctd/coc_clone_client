/**
 * Created by CPU02302_LOCAL on 12/12/2018.
 */

CentralLayer.prototype.addBuilding = function(buildingID, objBuilding) {
    var buildTypeID = objBuilding.type;
    var isoAt = cc.p(objBuilding["posX"], objBuilding["posY"]);
    var level = objBuilding.level;
    var state = objBuilding["isUpdate"]; //0: ko up, 1: dang up, 2: dang create
    var finishTime = objBuilding["buildTime"];

    // buildTypeID, isoAt, buildingID, level
    var buildingBaseData = baseGameBuildingInfo[buildTypeID][level];
    if (buildingBaseData) {
        var buildSize = buildingBaseData["height"];
        var outPos = this.positionForIsoAt(isoAt);
        var newBuilding = new BaseBuilding();
        newBuilding.setMapIndex(isoAt);
        newBuilding.init(buildTypeID, buildingID, buildSize, level, state, finishTime);
        newBuilding.setAnchorPoint(0.5, 1);
        newBuilding.setPosition(outPos.x + BUILDINGBASEGRID_WIDTH/2 , outPos.y - buildSize*BUILDINGBASEGRID_HEIGHT/2);

        this.MarkGridBuildInfo(isoAt, buildSize, buildingID, newBuilding);
        tmxTiledMap.addChild(newBuilding);
        this.setZOrderByIso(newBuilding);
    }
};

CentralLayer.prototype.addObs = function(obsID, objObs) {
    var obsType = objObs['type'];
    var isoAt = cc.p(objObs["posX"], objObs["posY"]);
    var buildingBaseData = baseGameBuildingInfo[obsType]['1']; // level alway is 1
    if (buildingBaseData) {
        var buildSize = buildingBaseData["width"];
        var outPos = this.positionForIsoAt(isoAt);
        var baseObs = new BaseObs();
        baseObs.init(obsType, obsID, buildSize);
        baseObs.setAnchorPoint(0.5, 1);
        baseObs.setPosition(outPos.x + BUILDINGBASEGRID_WIDTH/2 , outPos.y - buildSize*BUILDINGBASEGRID_HEIGHT/2);
        baseObs.setMapIndex(isoAt);

        this.MarkGridBuildInfo(isoAt, buildSize, obsID, baseObs);

        tmxTiledMap.addChild(baseObs);
        this.setZOrderByIso(baseObs);
    }
};
