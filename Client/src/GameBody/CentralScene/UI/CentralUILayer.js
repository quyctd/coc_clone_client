
var CentralUILayer = cc.Layer.extend({

    ctor: function() {
        this._super();
        this.init();
    },

    init: function() {

        winSize = cc.director.getWinSize();
        cc.spriteFrameCache.addSpriteFrames(res.ActionBuilding_plist);

        var sprite = new cc.Sprite("#bg_button.png");
        this.baseHeight = sprite.height/2 * 1.5;
        this.baseWidth = sprite.width/2 * 1.5;
        this.padding = 15;

        this.bgSprite = new cc.Sprite();
        this.addChild(this.bgSprite, 1);

        this.nameBuilding = new cc.LabelBMFont("", "res/Fonts/soji_20.fnt");
        this.addChild(this.nameBuilding, 1);

        this.addAllBtn();
    },

    addAllBtn: function() {
        var listAllBtn = gv.uiBtnConfig["ALL"];
        var i;
        for (i in listAllBtn) {
            var key = listAllBtn[i];
            this.addBtnByKey(key, gv.uiTranslater[key]);
        }
    },

    addBtnByKey: function(key, translateKey) {

        this[key] = new cc.Sprite();
        this[key].setPosition(-100, -100);
        this.addChild(this[key]);

        var strFile = key + "_icon.png";
        this[key].imageBtn = new ccui.Button();
        this[key].imageBtn.loadTextures(strFile, strFile, strFile, ccui.Widget.PLIST_TEXTURE);
        this[key].imageBtn.addTouchEventListener(this[key + "_touchEvent"], this);
        this[key].imageBtn.scale = 1.5;
        this[key].addChild(this[key].imageBtn, 1);

        var text = new cc.LabelBMFont(translateKey, "res/Fonts/soji_16.fnt");
        text.setPosition(this[key].imageBtn.x, this[key].imageBtn.y - 30);
        this[key].addChild(text, 2);

    },

    InfoBtnAppear: function(building) {
        var lastLevel = Object.keys(baseGameBuildingInfo[building.uid]).length;
        var keyGetBtn = (building.stateBuild === BUILDING_STATUS.BUILDING_FREE)?building.uid:(building.level===0?"RemoveObs":"Upgrade");

        var listBtn = JSON.parse(JSON.stringify(gv.uiBtnConfig[keyGetBtn])); // clone data from gv.uiBtnConfig to new variable
        //if in last level, number of btn down 1
        if (building.level === lastLevel) {
            var j = 0;
            for (var i = 0, l = listBtn.length; i < l; i++) {
                if (listBtn[i] !== "upgrade") {
                    listBtn[j++] = listBtn[i];
                }
            }
            listBtn.length = j;
        }
        var listPos = this.judgePosByLength(listBtn.length);

        for (var i = 0; i < listBtn.length; i++) {
            var str = listBtn[i];
            var pos = listPos[i];

            if (str === "upgrade" || str === "remove" || str === "quick_finish")
                this.addLobbyNeeded(str);

            this[str].setVisible(true);
            //this[str].setPosition(pos);
            this[str].stopAllActions();
            this[str].setPosition(pos.x, -100);
            this[str].runAction(cc.moveTo(0.2, cc.p(pos.x, pos.y)));
        }

        this.nameBuilding.setString( building.b_ID >= 100 ?  (gv.buildInfo[building.uid].name + " cấp " + building.level) : (gv.ObsInfo[building.uid].name));
        this.nameBuilding.setPosition(winSize.width/2, listPos[0].y + this.baseHeight + this.nameBuilding.height/2 + 10);
    },

    InfoBtnDisappear : function(building) {
        var lastLevel = Object.keys(baseGameBuildingInfo[building.uid]).length;
        var keyGetBtn = (building.stateBuild === BUILDING_STATUS.BUILDING_FREE)?building.uid:(building.level===0?"RemoveObs":"Upgrade");

        var listBtn = gv.uiBtnConfig[keyGetBtn];
        for (var i = 0; i< listBtn.length; i++) {
            var str = listBtn[i];
            if (building.level === lastLevel && listBtn[i] === "upgrade")
                continue;
            this[str].stopAllActions();
            this[str].runAction(cc.moveTo(0.2, cc.p(this[str].x, -100)));

            if (str === "upgrade" || str === "remove" || str === "quick_finish")
                this.hideLobbyNeeded(str);
        }
        this.nameBuilding.setString("");
    },

    addLobbyNeeded: function(key) {

        var btn = this[key];
        var building =  gv.centralScene.getChildByName("screen").m_building;

        if (key === "upgrade" || key === "remove")
        {
            var baseBuildInfo = building.getCostRequired();
            for (var i = 0; i < gv.upgradeCost.length; i++) {
                var upgradeType = gv.upgradeType[i];

                var lobbyNeeded = new cc.LabelBMFont(baseBuildInfo[upgradeType], "res/Fonts/soji_16.fnt");
                lobbyNeeded.setName(key + i + "lobbyNeeded");
                btn.addChild(lobbyNeeded, 2);

                var icon = new cc.Sprite("#"+gv.upgradeType[i]+"_icon.png");
                icon.setName(key + i + "icon");
                btn.addChild(icon, 3);

                lobbyNeeded.setPosition(btn.imageBtn.x - icon.width/2 - 5, btn.imageBtn.y + btn.imageBtn.height/2 * btn.imageBtn.scale - lobbyNeeded.height/2 - lobbyNeeded.height * i - 5 * i - 10);
                icon.setPosition(lobbyNeeded.x + lobbyNeeded.width/2 + 5 + icon.width/2, lobbyNeeded.y);

                if (baseBuildInfo[upgradeType] > gv.userData[upgradeType])
                    lobbyNeeded.color = new cc.Color(229, 78, 61);
                if (gv.acceptUpgrade)
                    lobbyNeeded.color = new cc.Color(255, 255, 255);
            }
        }

        if (key === "quick_finish") {
            var timeLeft = building.timeBuild - building["count"];
            var coin = Util.timeToCoin(timeLeft);
            lobbyNeeded = new cc.LabelBMFont(coin, "res/Fonts/soji_16.fnt");
            lobbyNeeded.setName(key + "lobbyNeeded");
            lobbyNeeded.setPosition(btn.imageBtn.x, btn.imageBtn.y + btn.imageBtn.height/2 * btn.imageBtn.scale - lobbyNeeded.height/2 - 10);
            btn.addChild(lobbyNeeded, 2);
        }
    },

    hideLobbyNeeded: function(key) {
        if (key !== "quick_finish") {
            for (var i =0; i < gv.upgradeCost.length; i++) {
                this[key].removeChildByName(key + i + "lobbyNeeded");
                this[key].removeChildByName(key + i + "icon");
            }
        }
        else
            this[key].removeChildByName(key + "lobbyNeeded");
    },

    resetBtn: function(building) {
        this.InfoBtnDisappear(building);
        this.InfoBtnAppear(building);
    },

    upgrade_touchEvent: function(sender, type) {
        switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                var centralLayer = gv.centralScene.getChildByName("screen");
                var touchBuilding = centralLayer.m_building;
                this.runEffect("upgrade");
                gv.centralScene.showDialog(touchBuilding, "upgrade");


                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    remove_touchEvent: function(sender, type) {
        switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                var centralLayer = gv.centralScene.getChildByName("screen");

                if (gv.userData[LOBBY.BUILDER] <= 0) {

                    gv.centralScene.showNoti("maxBuilder", 100);

                }

                var send = true;
                for (var i = 0; i < gv.upgradeCost.length; i++) {
                    if (gv.userData[gv.upgradeType[i]] < gv.upgradeCost[i] || gv.userData[LOBBY.BUILDER] <= 0)
                        send = false;
                }

                if (send) {
                    testnetwork.connector.sendClearOBS(centralLayer.m_building.b_ID); //Ham tu goi update lobby
                }
                else {

                    if (gv.userData[gv.upgradeType[0]] < gv.upgradeCost[0])
                        gv.centralScene.showNoti("lackLobby", 100);
                    else if (gv.userData[LOBBY.BUILDER] <= 0)
                        gv.centralScene.showNoti("maxBuilder", 1);

                }

                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    info_touchEvent: function(sender, type) {
        switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                //Call show info building
                var touchBuilding = gv.centralScene.getChildByName("screen").m_building;
                this.runEffect("info");
                gv.centralScene.showDialog(touchBuilding, "info");
                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    harvest_gold_touchEvent: function(sender, type) {
        switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                //Call Coming soon
                gv.centralScene.showError("Coming soon...");
                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    share_touchEvent: function(sender, type) {
        switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                //Call Coming soon
                gv.centralScene.showError("Coming soon...");

                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    harvest_elixir_touchEvent: function(sender, type) {
        switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                //Call Coming soon
                gv.centralScene.showError("Coming soon...");

                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    harvest_darkElixir_touchEvent: function(sender, type) {
        switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                //Call Coming soon
                gv.centralScene.showError("Coming soon...");

                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    clan_button_touchEvent: function(sender, type) {
        switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                //Call Coming soon
                gv.centralScene.showError("Coming soon...");

                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    quick_finish_touchEvent: function(sender, type) {
        switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                //Call show info building
                var touchBuilding = gv.centralScene.getChildByName("screen").m_building;
                this.runEffect("quick_finish");
                var timeLeft = touchBuilding.timeBuild - touchBuilding.count;
                var coin = Util.timeToCoin(timeLeft);
                if (coin <= gv.userData[LOBBY.COIN])
                    gv.centralScene.showNoti("quick_finish", coin);
                else
                    gv.centralScene.showNoti("lackCoin", coin);

                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    cancel_touchEvent: function(sender, type) {
        switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                //Call show info building
                var touchBuilding = gv.centralScene.getChildByName("screen").m_building;
                this.runEffect("cancel");
                gv.centralScene.showNoti("cancel", -1);
                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    judgePosByLength: function(length) {
        var listPos = [];
        if (length % 2 === 0) {
            var count =  - length/2 ;
            var order = count;
            for (var i = 0; i < length; i++) {
                listPos[i] = cc.p(winSize.width/2  + this.padding * order + this.baseWidth * (2*(order + 1) - 1), 30 + this.baseHeight);
                order+= 1;
            }
        }

        else {
            var count = Math.ceil(-length/2);
            var order = count;
            for (var i = 0; i < length; i ++) {
                listPos[i] = cc.p(winSize.width/2 + this.padding * order + this.baseWidth * 2 * order, 30 + this.baseHeight);
                order++;
            }
        }

        return listPos;
    },

    runEffect: function(key) {
        cc.audioEngine.playEffect( gameRes.button_click);
        var seqScale = cc.sequence(cc.scaleTo(0.125, 1.3), cc.scaleTo(0.125, 1));
        this[key].runAction(seqScale);
    }

});


