/**
 * Created by CPU02302_LOCAL on 12/10/2018.
 */

var LobbyLayer = cc.Layer.extend({

    timeToAddOver : 0.2, // seconds

    ctor: function() {
        this._super();
        this.init();
    },

    init: function() {
        //init
        winSize = cc.director.getWinSize();
        this.padding = 30;
        this.baseGridWidth = (winSize.width - this.padding*2) / 8;
        this.baseGridHeight = (winSize.height - this.padding*2) / 9;
        this.loadLobbyData();
        cc.spriteFrameCache.addSpriteFrames(res.mainGUI_plist);

        this.addShopButton();

        this.addResource();
        this.addUserLobby();

        this.addCheatButton();
        this.addButtonSound();
    },

    loadLobbyData : function() {
        //load init file
        var initGameData = {};
        cc.loader.loadJson(res.initGameData, function(err, ret) {
            initGameData = ret;
        });
        if (!gv.userData)
            gv.userData = initGameData['player'];
    },

    addShopButton: function () {
        //load ShopButton

        var shop = new ccui.Button();
        shop.loadTextures("shop.png", "shop.png", "shop.png", ccui.Widget.PLIST_TEXTURE);
        shop.scale = 1.5;
        shop.addTouchEventListener(this.shop_touchEvent, this);
        shop.setPosition(this.baseGridWidth * 8 + this.padding - shop.getContentSize().width/2 * shop.getScale() , this.padding + shop.getContentSize().height/2 * shop.getScale());
        shop.setName("Shop");
        this.addChild(shop, 1);
    },

    shop_touchEvent: function(sender, type) {
        switch (type) {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;
            case ccui.Widget.TOUCH_ENDED:
                var shopLayer = new ShopLayer();
                shopLayer.setName("Shop");
                cc.director.getRunningScene().addChild(shopLayer, 4);
                gv.centralScene.pauseLayer();
                break;
            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },
    
    addResource: function () {
        //Load Lobby Resource UI

        this.addLobbyByType("gold");
        this["gold"].setPosition(winSize.width - 70, winSize.height - 70);

        this.addLobbyByType("elixir");
        this["elixir"].setPosition(winSize.width - 70, winSize.height - 70 * 2);

        this.addGem();
        this["coin"].setPosition(winSize.width - 70, winSize.height - 70 * 3);

        if (gv.userData[LOBBY.DARK_ELIXIR] !== undefined && gv.userData[LOBBY.MAX_DARK_ELIXIR] !== 0) {
            this.addLobbyByType("darkElixir");
            this["darkElixir"].setPosition(winSize.width - 70, winSize.height - 70 * 3);
            this["coin"].setPosition(winSize.width - 70, winSize.height - 70 * 4);
        }
    },

    addLobbyByType: function(type) {

        this[type] = new cc.Sprite();
        this[type].scale = 2;
        this.addChild(this[type], 1);

        //icon
        var icon = new cc.Sprite("#" + type + "_icon.png");
        this[type].addChild(icon, 1);

        //background bar
        var bg_bar = new cc.Sprite("#bg_bar_2.png");
        bg_bar.setPosition(icon.x - icon.width/2 - bg_bar.width/2 - 10, icon.y);
        this[type].addChild(bg_bar, 1);

        var maxType = "max" + type.charAt(0).toUpperCase() + type.slice(1);

        //loading bar
        this[type].bar = new ccui.LoadingBar();
        this[type].bar.loadTexture(type + "_bar.png", ccui.Widget.PLIST_TEXTURE);
        this[type].bar.attr({
            direction: ccui.LoadingBar.TYPE_RIGHT,
            x: bg_bar.x - 4,
            y: bg_bar.y + 3,
            percent: gv.userData[type]/gv.userData[maxType] * 100
        });

        this[type].addChild(this[type].bar, 2);

        //draw text current
        this[type].textCur = new cc.LabelBMFont(gv.userData[type].toString(), "res/Fonts/soji_16.fnt");
        this[type].textCur.attr({
            scale: 0.5,
            anchorX: 1,
            anchorY: 0.5,
            x: this[type].bar.x + this[type].bar.width/2 - 5,
            y: this[type].bar.y
        });

        this[type].addChild(this[type].textCur, 3);

        //draw text max
        this[type].textMax = new cc.LabelBMFont("Tối đa: " + gv.userData[maxType].toString(), "res/Fonts/soji_16.fnt");
        this[type].textMax.attr({
            scale: 0.5,
            anchorX: 0,
            anchorY: 0,
            x: bg_bar.x - bg_bar.width/2,
            y: bg_bar.y + bg_bar.height/2
        });

        this[type].addChild(this[type].textMax, 3);
    },

    addGem: function() {
        //load gem
        this["coin"] = new cc.Sprite();
        this["coin"].scale = 2;
        this.addChild(this["coin"], 1);

        var icon = cc.Sprite("#g_icon.png");
        this["coin"].addChild(icon, 1);

        var bg_bar = cc.Sprite("#bg_bar_2.png");
        bg_bar.setPosition(icon.x - icon.width/2 - bg_bar.width/2 - 10, icon.y);
        this["coin"].addChild(bg_bar, 1);

        //add Text
        this["coin"].textCur = new cc.LabelBMFont(gv.userData.coin.toString(), "res/Fonts/soji_16.fnt");
        this["coin"].textCur.attr({
            scale: 0.5,
            anchorX: 1,
            anchorY: 0.5,
            x: bg_bar.x + bg_bar.width/2 - 4 - 10,
            y: bg_bar.y + 3
        });
        this["coin"].addChild(this["coin"].textCur, 3);

    },

    addUserLobby: function () {
        //Load Lobby User UI
        this.addLevelLobby();
        this["level"].setPosition(this.padding + 120, winSize.height - this.padding - 50);
        //
        this.addTrophyLobby();
        this["trophy"].setPosition(this.padding + 120, winSize.height - this.padding - 50 - 70);

        this.addTroopLobby("builder");
        this["builder"].setPosition(winSize.width/2, winSize.height - this.padding - 30);
        this["builder"].text.setString(gv.userData[LOBBY.BUILDER].toString() + "/" + gv.userData[LOBBY.MAX_BUILDER].toString());

        this.addTroopLobby("army");
        this["army"].setPosition(winSize.width/2 - 250, winSize.height - this.padding - 30);
        this["army"].text.setString("0/25");

        this.addTroopLobby("shield");
        this["shield"].setPosition(winSize.width/2 + 250, winSize.height - this.padding - 30);
        this["shield"].text.setString("0s");


    },

    addLevelLobby: function() {
        //load level
        this["level"] = new cc.Sprite();
        this["level"].scale = 2;
        this.addChild(this["level"], 1);

        var bg = cc.Sprite("#trophy_bg_bar.png");
        this["level"].addChild(bg, 1);

        var icon = cc.Sprite("#exp_icon.png");
        icon.attr({
           anchorX: 0,
           anchorY: 0.5,
           x:  bg.x - bg.width/2,
            y: bg.y + 10
        });
        this["level"].addChild(icon, 2);

        this["level"].level = new cc.LabelBMFont(gv.userData.level.toString(), "res/Fonts/soji_20.fnt");
        this["level"].level.scale = 0.5;
        this["level"].level.setPosition(icon.x + icon.width/2, icon.y);
        this["level"].addChild(this["level"].level, 3);

        //Add username
        var username = new cc.LabelBMFont(gv.userData.username.toString(), "res/Fonts/soji_16.fnt");
        username.attr({
            scale: 0.5,
            anchorX: 0,
            anchorY: 0.5,
            x: icon.x + icon.width + 2,
            y: bg.y + bg.height/2 + 5
        });
        this["level"].addChild(username, 3);

        this["level"].text = new cc.LabelBMFont(gv.userData.exp.toString(), "res/Fonts/soji_16.fnt");
        this["level"].text.attr({
            scale : 0.5,
            anchorX: 0,
            anchorY: 0.5,
            x: icon.x + icon.width + 2,
            y: bg.y
        });
        this["level"].addChild(this["level"].text, 3);

    },

    addTrophyLobby: function() {
        //load trophy
        this["trophy"] = new cc.Sprite();
        this["trophy"].scale = 2;
        this.addChild(this["trophy"], 1);

        var bg = new cc.Sprite("#trophy_bg_bar.png");
        this["trophy"].addChild(bg, 1);

        var icon = new cc.Sprite("#trophy.png");
        icon.attr({
            anchorX: 0,
            anchorY: 0.5,
            x:  bg.x - bg.width/2 + 8,
            y: bg.y + 3
        });
        this["trophy"].addChild(icon, 2);

        this["trophy"].text = new cc.LabelBMFont(gv.userData.trophy.toString(), "res/Fonts/soji_16.fnt");
        this["trophy"].text.attr({
            scale : 0.5,
            anchorX: 0,
            anchorY: 0.5,
            x: icon.x + icon.width + 6,
            y: bg.y
        });
        this["trophy"].addChild(this["trophy"].text, 3);
    },

    addTroopLobby: function(type) {
        this[type] = new cc.Sprite();
        this[type].scale = 2;
        this.addChild(this[type], 1);

        var bg = cc.Sprite("#bg_bar_1.png");
        this[type].addChild(bg, 1);

        var icon = cc.Sprite(type === "shield"?"#shield.png":"#" + type + "_icon.png");
        icon.attr({
            anchorX: 0.5,
            anchorY: 0.5,
            x: bg.x - bg.width/2,
            y: bg.y
        });
        this[type].addChild(icon, 2);

        this[type].text = new cc.LabelBMFont("",  "res/Fonts/soji_16.fnt");
        this[type].text.scale = 0.5;
        this[type].text.setPosition(bg.x, bg.y);
        this[type].addChild(this[type].text, 2);

        var label = new cc.LabelBMFont(gv.uiTranslater[type], "res/Fonts/soji_16.fnt");
        label.attr({
            scale: 0.5,
            anchorX: 0.5,
            anchorY: 0,
            x: bg.x,
            y: bg.y + bg.height/2
        });
        this[type].addChild(label, 2);

    },

    addCheatButton: function() {
        var cheat = new ccui.Button();
        cheat.loadTextures("res/Arts/GUIs/button.png", "res/Arts/GUIs/button.png", "res/Arts/GUIs/button.png");
        // cheat.setString("Cheat");
        cheat.scale = 1.5;
        cheat.addTouchEventListener(this.cheat_touchEvent, this);
        cheat.setPosition( this.padding + cheat.getContentSize().width/2 * cheat.getScale() , this.padding + cheat.getContentSize().height/2 * cheat.getScale());
        cheat.setName("Cheat");
        this.addChild(cheat, 1);

        var cheatText = new cc.LabelBMFont("CHEAT COIN", "res/Fonts/soji_16.fnt");
        cheatText.setPosition(cheat.x, cheat.y);
        this.addChild(cheatText, 2);
    },

    cheat_touchEvent: function(sender, type) {
        switch (type) {
            case ccui.Widget.TOUCH_BEGAN:
                break;
            case ccui.Widget.TOUCH_MOVED:
                break;
            case ccui.Widget.TOUCH_ENDED:
                testnetwork.connector.sendCheatLobby();
                break;
            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    addButtonSound: function() {
        var sound = new ccui.Button();
        sound.loadTextures("res/Arts/GUIs/button.png", "res/Arts/GUIs/button.png", "res/Arts/GUIs/button.png");
        var shop = this.getChildByName("Shop");
        sound.addTouchEventListener(this.sound_touchEvent, this);
        sound.setPosition( shop.x , shop.y + shop.height/2 * shop.scale + 10 + sound.height/2);
        this.addChild(sound, 1);

        var state = gv.sound?"On":"Off";

        var soundText = new cc.LabelBMFont("Music: " + state, "res/Fonts/soji_16.fnt");
        soundText.setPosition(sound.x, sound.y);
        soundText.setName("soundText");
        this.addChild(soundText, 2);
    },

    sound_touchEvent: function(sender, type) {
        switch (type) {
            case ccui.Widget.TOUCH_BEGAN:
                break;
            case ccui.Widget.TOUCH_MOVED:
                break;
            case ccui.Widget.TOUCH_ENDED:
                gv.sound = !gv.sound;
                var state;
                if (gv.sound) {
                    state = "On";
                    cc.audioEngine.setMusicVolume(0.5);
                    cc.audioEngine.playMusic(gameRes.bgMusic_mp3, true);
                }
                else {
                    state = "Off";
                    cc.audioEngine.stopMusic();
                    cc.audioEngine.stopAllEffects();
                }
                this.getChildByName("soundText").setString("Music: " + state);
                break;
            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    }
});