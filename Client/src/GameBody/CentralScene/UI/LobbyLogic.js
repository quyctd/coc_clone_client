LobbyLayer.prototype.update = function (type) {
    switch (type) {
        case LOBBY.GOLD:
            this.updateGold();
            break;
        case LOBBY.ELIXIR:
            this.updateElixir();
            break;
        case LOBBY.DARK_ELIXIR:
            this.updateDarkElixir();
            break;
        case LOBBY.COIN:
            this.updateCoin();
            break;
        case LOBBY.BUILDER:
            this.updateBuilder();
            break;
        case LOBBY.LEVEL:
            this.updateLevel();
            break;
        case LOBBY.TROPHY:
            this.updateTrophy();
            break;
        case LOBBY.ARMY:
            this.updateArmy();
            break;
        case LOBBY.ALL:
            this.updateAll();
            break;
    }
};

LobbyLayer.prototype.updateAll = function() {
    this.updateGold();
    this.updateElixir();
    this.updateCoin();
    this.updateDarkElixir();
    this.updateBuilder();
    this.updateLevel();
    this.updateTrophy();
    this.updateArmy();
};

LobbyLayer.prototype.updateGold = function () {

    this["gold"].bar.setPercent(gv.userData.gold / gv.userData["maxGold"] * 100);
    this["gold"].textCur.setString(gv.userData.gold.toString());
    this["gold"].textMax.setString("Tối đa: " + gv.userData["maxGold"].toString());

};

LobbyLayer.prototype.updateElixir = function () {

    this["elixir"].bar.setPercent(gv.userData.elixir / gv.userData["maxElixir"] * 100);
    this["elixir"].textCur.setString(gv.userData.elixir.toString());
    this["elixir"].textMax.setString("Tối đa: " + gv.userData["maxElixir"].toString());

};

LobbyLayer.prototype.updateCoin = function () {
    this["coin"].textCur.setString(gv.userData.coin.toString());
};

LobbyLayer.prototype.updateDarkElixir = function () {

    if(gv.userData["maxDarkElixir"]) {
        this["darkElixir"].setVisible(true);

        this["coin"].setPosition(winSize.width - 70, winSize.height - 70 * 4);

        this["darkElixir"].bar.setPercent( gv.userData.darkElixir / gv.userData["maxDarkElixir"] * 100);

        this["darkElixir"].textCur.setString(gv.userData.darkElixir.toString());

        this["darkElixir"].textMax.setString("Tối đa: " + gv.userData["maxDarkElixir"].toString());
    }
};

LobbyLayer.prototype.updateBuilder = function () {
    this["builder"].text.setString(gv.userData[LOBBY.BUILDER].toString() + "/" + gv.userData[LOBBY.MAX_BUILDER].toString());
};

LobbyLayer.prototype.updateLevel = function () {
    this["level"].level.setString(gv.userData.level.toString());

    this["level"].text.setString(gv.userData.exp.toString());
};

LobbyLayer.prototype.updateTrophy = function () {
    this["trophy"].text.setString(gv.userData["trophy"].toString());
};

LobbyLayer.prototype.updateArmy = function () {
    this["army"].text.setString("0/25");
};
