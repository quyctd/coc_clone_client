/**
 * Created by CPU10410_LOCAL on 1/2/2019.
 */

/**
 * Add building:
 * Khi a click vào building mua được -> lấy ra buildingType của building đấy r dùng những api dưới đây là sẽ add đc nhà.
 * var baseBuildingInfo = baseGameBuildingInfo[buildTypeID]["1"] // buildTypeID: "RES_1", "1" là level
 * Dùng hàm pos = generatePosCanPutDownBuild(buildSize) để lấy pos đặt nhà. //buildSize = baseBuildingInfo["height"]; Hàm này trong CentralLogic.js
 * var objBuilding = {"type": buildingType,"posX": pos.x,"posY": pos.y,"isUpdate": 2,"time":0,"level":1,"id":0}; //isUpdate = 2 để create nhà mới
 * Gọi đến hàm addBuilding(gv.lastBuildID + 1, objBuilding) trong CreateWorld.js // ID sẽ tăng dần mỗi khi tạo nhà mới
 * Thế là xong
 **/

var CustomTableViewCell = cc.TableViewCell.extend({
    draw:function (ctx) {
        this._super(ctx);
    }
});

var cnt =0;
var category = [
    //category
    [
        {label:"Ngân Khố",imageName:"type_buy_res"},
        {label:"Tài Nguyên",imageName:"type_res"},
        {label:"Trang Trí",imageName:"type_dc"},
        {label:"Quân Đội",imageName:"type_army"},
        {label:"Phòng Thủ",imageName:"type_defense"},
        {label:"Bảo Vệ",imageName:"type_sheild"}
    ],
    //Ngân khố
    [
    ],
    // Tài nguyên
    [
        {label:"Mỏ vàng",imageName:"RES_1"},
        {label:"Mỏ dầu",imageName:"RES_2"},
        {label:"Kho vàng",imageName:"STO_1"},
        {label:"Kho dầu",imageName:"STO_2"},
        {label:"Kho dầu đen",imageName:"STO_3"},
        {label:"Mỏ dầu đen",imageName:"RES_3"}
    ],
    //Trang Trí
    [
        {label:"Tượng cung thủ",imageName:"DEC_1"},
        {label:"Tượng chiến binh",imageName:"DEC_2"},
        {label:"Cờ hiệu 1",imageName:"DEC_3"},
        {label:"Cờ hiệu 2",imageName:"DEC_4"},
        {label:"Hoa đỏ",imageName:"DEC_5"},
        {label:"Hoa cúc",imageName:"DEC_6"}

    ],
    //Quân đội
    [
        {label:"Trại lính",imageName:"AMC_1"},
        {label:"Nhà thợ xây",imageName:"BDH_1"},
        {label:"Nhà lính",imageName:"BAR_1"},
        {label:"Nhà X-men",imageName:"BAR_2"},
        {label:"Hoang đế",imageName:"KQB_1"},
        {label:"Nữ hoàng",imageName:"KQB_2"},
        {label:"Đền thờ",imageName:"KQB_3"},
        {label:"Nhà phép thuật",imageName:"SPF_1"},
        {label:"Nhà nghiên cúu",imageName:"LAB_1"}
    ],
    //Phòng thủ
    [
        {label:"Pháo",imageName:"DEF_1"},
        {label:"Chòi cung",imageName:"DEF_2"},
        {label:"Máy bắn đá",imageName:"DEF_3"},
        {label:"Chòi phép",imageName:"DEF_4"},
        {label:"Pháo cao xạ",imageName:"DEF_5"},
        {label:"Máy bắn tên",imageName:"DEF_7"},
        {label:"Cột điện",imageName:"DEF_8"},
        {label:"Bom",imageName:"TRA_1"},
        {label:"Nắm đấm",imageName:"TRA_2"},
        {label:"Kho thuốc súng",imageName:"TRA_3"},
        {label:"Mìn tầm nhiệt",imageName:"TRA_4"},
        {label:"Bẫy xương",imageName:"TRA_6"},
        {label:"Tường",imageName:"WAL_1"},
        {label:"Tháp Laser",imageName:"DEF_9"}
    ],
    //Bảo vệ
    [
        {label:"Bảo vệ 1 ngày",imageName:"SHEILD_1"},
        {label:"Bảo vệ 2 ngày",imageName:"SHEILD_2"},
        {label:"Bảo vệ 7 ngày",imageName:"SHEILD_3"}
    ],
];
var ShopLayer = cc.LayerColor.extend({

    tableView : null,
    ctor:function () {
        this.currentPos = cc.p(42, 42);
        this._super();
        this.setColor(cc.color( 33, 33, 33 , 1));
        this.indx =0;
        this.gold = gv.userData.gold;
        this.coin = gv.userData.coin;
        this.elixir = gv.userData.elixir;
        var winSize = cc.Director.getInstance().getWinSize();

        // title
        var sprite = cc.Sprite("res/Arts/GUIs/shop_gui/res_info.png");
        //sprite.setAnchorPoint(cc.p(0,0));
        sprite.setPosition(cc.p(winSize.width/2, winSize.height*23/24));
        sprite.setScale(winSize.width/sprite.width,winSize.height/sprite.height/12);
        this.addChild(sprite);
        // BACK BUTTON
        var Back = new ccui.Button();
        Back.loadTextures("res/Arts/GUIs/shop_gui/back.png", "res/Arts/GUIs/shop_gui/back.png", "res/Arts/GUIs/shop_gui/back.png", ccui.Widget.LOCAL_TEXTURE);
        Back.setScale(winSize.width/Back.width/15,winSize.height/Back.height/15);
        Back.addTouchEventListener(this.back_touchEvent, this);
        Back.setPosition(winSize.width*9/200,winSize.height*229/240);
        Back.setTag(2);
        Back.setVisible(false);
        this.addChild(Back, 1);
        // close button
        var Close = new ccui.Button();
        Close.loadTextures("res/Arts/GUIs/shop_gui/close.png", "res/Arts/GUIs/shop_gui/close.png", "res/Arts/GUIs/shop_gui/close.png", ccui.Widget.LOCAL_TEXTURE);
        Close.setScale(winSize.width/Close.width/15,winSize.height/Close.height/15)
        Close.addTouchEventListener(this.close_touchEvent, this);
        Close.setPosition(winSize.width*193/200,winSize.height*229/240);
        this.addChild(Close, 1);

        // label title
        var labelTitle = new cc.LabelBMFont("Cửa Hàng", "res/Fonts/soji_24.fnt");// label trên header
        //labelTitle.setColor(cc.color(155, 25, 25));
        labelTitle.setPosition(winSize.width/2, winSize.height*23/24);
        labelTitle.setTag(3);
        this.addChild(labelTitle);
        //res info
        //cc.director.setClearColor(cc.color(153, 255, 255, 1));


        var bottomSprite  = cc.Sprite();
        var sprite = cc.Sprite("res/Arts/GUIs/shop_gui/res_info.png");
        sprite.setPosition(cc.p(winSize.width/2, winSize.height/24));
        sprite.setScale(winSize.width/sprite.width,winSize.height/sprite.height/12);
        bottomSprite.addChild(sprite);
        var sprite = cc.Sprite("res/Arts/GUIs/shop_gui/res_bar.png");
        sprite.setPosition(cc.p(winSize.width/4,winSize.height/24));
        bottomSprite.addChild(sprite);cc.director.setClearColor(cc.color(153, 255, 255, 1));
        var sprite = cc.Sprite("res/Arts/GUIs/shop_gui/res_bar.png");
        sprite.setPosition(cc.p(winSize.width*2/4,winSize.height/24));
        bottomSprite.addChild(sprite);cc.director.setClearColor(cc.color(153, 255, 255, 1));
        var sprite = cc.Sprite("res/Arts/GUIs/shop_gui/res_bar.png");
        sprite.setPosition(cc.p(winSize.width*3/4,winSize.height/24));
        bottomSprite.addChild(sprite);
        var sprite = cc.Sprite("res/Arts/GUIs/shop_gui/icon_gold_bar.png");
        sprite.setPosition(cc.p(60+winSize.width/4,winSize.height/24));
        bottomSprite.addChild(sprite);cc.director.setClearColor(cc.color(153, 255, 255, 1));
        var sprite = cc.Sprite("res/Arts/GUIs/shop_gui/icon_elixir_bar.png");
        sprite.setPosition(cc.p(60+winSize.width*2/4,winSize.height/24));
        bottomSprite.addChild(sprite);cc.director.setClearColor(cc.color(153, 255, 255, 1));
        var sprite = cc.Sprite("res/Arts/GUIs/shop_gui/icon_g_bar.png");
        sprite.setPosition(cc.p(60+winSize.width*3/4,winSize.height/24));
        bottomSprite.addChild(sprite);
        // res number
        var label = cc.LabelTTF.create(this.gold.toFixed(), "Helvetica", 18.0);
        label.setPosition(cc.p(15+winSize.width/4,winSize.height/24));
        //label.setAnchorPoint(cc.p(0,0));
        bottomSprite.addChild(label);
        var label = cc.LabelTTF.create(this.elixir.toFixed(), "Helvetica", 18.0);
        label.setPosition(cc.p(15+winSize.width*2/4,winSize.height/24));
        //label.setAnchorPoint(cc.p(0,0));
        bottomSprite.addChild(label);
        var label = cc.LabelTTF.create(this.coin.toFixed(), "Helvetica", 18.0);
        label.setPosition(cc.p(15+winSize.width*3/4,winSize.height/24));
        //label.setAnchorPoint(cc.p(0,0));
        bottomSprite.addChild(label);
        bottomSprite.setTag(5);
        bottomSprite.setVisible(false);
        this.addChild(bottomSprite);


        this.tableView = cc.TableView.create(this, cc.size(winSize.width-20, winSize.height*3/4));
        this.tableView.setDirection(cc.SCROLLVIEW_DIRECTION_HORIZONTAL);
        this.tableView.setPosition(cc.p(10,winSize.height/4));
        this.tableView.setDelegate(this);
        this.addChild(this.tableView);
        this.tableView.reloadData();

    },
    back_touchEvent:function(sender, type){
        switch (type) {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;
            case ccui.Widget.TOUCH_ENDED:
                var Back = this.getChildByTag(2);
                Back.setVisible(false);
                var bottomSprite = this.getChildByTag(5);
                bottomSprite.setVisible(false);

                this.indx=0;
                this.tableView.setPosition(cc.p(10,winSize.height/4));
                this.tableView.reloadData();
                var labelTitle = this.getChildByTag(3);
                labelTitle.setString("Cửa Hàng");
                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    close_touchEvent:function(sender, type){
        switch (type) {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;
            case ccui.Widget.TOUCH_ENDED:
                cc.director.getRunningScene().removeChildByName("Shop");
                gv.centralScene.resumeLayer();
                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    tableCellTouched:function (table, cell) {
        //cc.log("this.indx="+this.indx);

        if (this.indx === 0) {
            this.indx = cell.getIdx() + 1;
            var labelTitle = this.getChildByTag(3);
            labelTitle.setString(category[0][this.indx - 1].label);
            var Back = this.getChildByTag(2);
            Back.setVisible(true);
            var bottomSprite = this.getChildByTag(5);
            bottomSprite.setVisible(true);
            table.setPosition(cc.p(10,winSize.height/8));
            table.reloadData();
        }
        else {

            var baseBuildingInfo = baseGameBuildingInfo[category[this.indx][cell.getIdx()].imageName]["1"];
            var pos = cc.director.getRunningScene().getChildByName("screen").generatePosCanPutDownBuild(baseBuildingInfo["height"]);
            if (this.currentPos !== pos) {
                var centralLayer = cc.director.getRunningScene().getChildByName("screen");
                if (centralLayer.m_building) {
                    gv.centralScene.UILayer.InfoBtnDisappear(centralLayer.m_building);
                    centralLayer.m_building.isShowClickInfo(false);
                }
                centralLayer.m_building = null;

                this.addBuildingFromShop(MAXBUILDINGID, category[this.indx][cell.getIdx()].imageName, pos.x, pos.y, 0);
                this.currentPos = pos;
                this.setVisible(false);
                gv.centralScene.resumeLayer();
            }
        }
    },

    addBuildingFromShop : function(id, type, posx, posy, timeLeft) {
        var objBuilding = {
            "type":type,
            "posX": posx,
            "posY": posy,
            "isUpdate": 2,
            "buildTime": timeLeft,
            "level": 1,
            "id": id
        };
        cc.director.getRunningScene().getChildByName("screen").addBuilding(id, objBuilding);
        // cc.log("ID new building", id);
        // var centralLayer = gv.centralScene.getChildByName("screen");
        // // centralLayer.scale = 1.5;
        // var newPos = centralLayer.positionForIsoAt(cc.p(posx, posy));
        // cc.log("Pos in map: ", newPos.x.toString(), newPos.y);
        // var wSpace = tmxTiledMap.convertToWorldSpace(newPos);
        // var posOfBuilding = centralLayer.convertToNodeSpace(wSpace);
        // cc.log("Pos in layer: ", posOfBuilding.x.toString(), posOfBuilding.y);
        //
        // centralLayer.setPosition(posOfBuilding.x - winSize.width/2, posOfBuilding.y - winSize.height/2 + 3 * BUILDINGBASEGRID_HEIGHT);
    },

    isAbleToBuy:function(buildingType){

        if (baseGameBuildingInfo[buildingType]== undefined)
            return false;
        var coinNeed = baseGameBuildingInfo[buildingType]["1"]["coin"];
        var goldNeed = baseGameBuildingInfo[buildingType]["1"]["gold"];
        var elixirNeed = baseGameBuildingInfo[buildingType]["1"]["elixir"];

        if (coinNeed == undefined)
            coinNeed =0;
        if (goldNeed == undefined)
            goldNeed =0;
        if (elixirNeed == undefined)
            elixirNeed =0;
        cc.log("Debuging " + buildingType+" "+coinNeed + " " + goldNeed + " " + elixirNeed);
        if (coinNeed > this.coin)
            return false;
        if (goldNeed > this.gold)
            return false;
        if (elixirNeed > this.elixir)
            return false;
        return true;
    },

    tableCellSizeForIndex:function (table, idx) {
        if (this.indx==0)
            return cc.size(winSize.width/3,winSize.height/2 );
        else
            return cc.size(winSize.width/3, winSize.height*3/4);
    },

    tableCellAtIndex:function (table, idx) {
        var strValue = idx.toFixed(0);
        var cell = table.dequeueCell();
        var label;
        var link = "res/Arts/GUIs/shop_gui/";
        if (this.indx!=0)
            link = "res/Arts/GUIs/icons/shop_gui/icon/";
        if (!cell) {
            cell = new CustomTableViewCell();

            var buildingType = category[this.indx][Number(strValue)].imageName;
            var sprite = null;
            if (this.indx!=0){
                sprite = cc.Sprite("res/Arts/GUIs/shop_gui/slot.png");
                sprite.setScale(winSize.width/sprite.width/3,winSize.height * 3 / sprite.height / 4);
            }
            else{
                sprite = cc.Sprite("res/Arts/GUIs/shop_gui/slot_catalogy.png");
                sprite.setScale(winSize.width/sprite.width/3,winSize.height/sprite.height/2);
            };
            sprite.setAnchorPoint(cc.p(0,0));
            //sprite.setPosition(cc.p(0, 0));
            sprite.setTag(1);
            cell.addChild(sprite);

            if (this.indx!=0) {
                sprite = cc.Sprite("res/Arts/GUIs/shop_gui/item_background.png");
                sprite.setScale(winSize.width/sprite.width/3,winSize.height * 3 / sprite.height / 4);
            }
            else {
                sprite = cc.Sprite("res/Arts/GUIs/shop_gui/catalogy_bg.png");
                sprite.setScale(winSize.width/sprite.width/3,winSize.height / sprite.height / 2);
            };
            sprite.setAnchorPoint(cc.p(0,0));
            //sprite.setPosition(cc.p(0, 0));
            sprite.setTag(4);

            cell.addChild(sprite);


            var sprite = cc.Sprite(link.concat(buildingType).concat(".png"));
            sprite.setAnchorPoint(cc.p(0,0));
            sprite.setTag(2);
            if (this.indx!=0) {
                sprite.setScale(winSize.width / sprite.width / 3, winSize.height * 3 / sprite.height / 4);
            }
            else {
                sprite.setScale(winSize.width / sprite.width / 3, winSize.height / sprite.height / 2);
            };
            cell.addChild(sprite);

            var sprite = cc.Sprite("res/Arts/GUIs/shop_gui/title_background.png");
            sprite.setAnchorPoint(cc.p(0,0));
            sprite.setPosition(cc.p(0, 0));
            sprite.setScale(winSize.width/sprite.width/3,winSize.height/sprite.height/10);
            sprite.setTag(6);
            if (this.indx!=0)
                sprite.setVisible(false);
            else
                sprite.setVisible(true);
            cell.addChild(sprite);


            var label = new cc.LabelBMFont(category[this.indx][Number(strValue)].label, "res/Fonts/soji_20.fnt");
            if (this.indx!=0) {
                label.setPosition(cc.p(winSize.width / 6, winSize.height * 27 / 40));
                if (baseGameBuildingInfo[buildingType]== undefined)
                    label.setString(category[this.indx][Number(strValue)].label+" undefined");
            }
            else {
                label.setPosition(cc.p(winSize.width / 6, 50));
            }
            label.setTag(3);
            cell.addChild(label);

            var sprite = cc.Sprite("res/Arts/GUIs/shop_gui/gold.png");
            sprite.setAnchorPoint(cc.p(0,0));
            sprite.setPosition(cc.p(450, 50));
            sprite.setTag(7);
            var cost =0;
            if (this.indx!=0) {
                sprite.setVisible(true);
                if (baseGameBuildingInfo[buildingType]!= undefined){
                    var coinNeed = baseGameBuildingInfo[buildingType]["1"]["coin"];
                    var goldNeed = baseGameBuildingInfo[buildingType]["1"]["gold"];
                    var elixirNeed = baseGameBuildingInfo[buildingType]["1"]["elixir"];

                    if (coinNeed == undefined)
                        coinNeed =0;
                    if (goldNeed == undefined)
                        goldNeed =0;
                    if (elixirNeed == undefined)
                        elixirNeed =0;
                    cost = goldNeed;
                    if (coinNeed!=0) {
                        cost = coinNeed;
                        sprite.setTexture("res/Arts/GUIs/shop_gui/g.png");
                    }
                    else if (elixirNeed!=0) {
                        sprite.setTexture("res/Arts/GUIs/shop_gui/elixir.png");
                        cost = elixirNeed;
                    }
                }
            }
            else
                sprite.setVisible(false);
            cell.addChild(sprite);

            var label  = new cc.LabelBMFont(cost.toString(), "res/Fonts/soji_20.fnt");
            label.setTag(8);
            label.setAnchorPoint(cc.p(0,0));
            label.setPosition(cc.p(320, 50));
            if (this.indx!=0) {
                if (this.isAbleToBuy(buildingType) != true)
                    label.setColor(cc.color(255, 32, 32));
                else
                    label.setColor(cc.color(255, 255, 255));
                label.setVisible(true);
            }
            else
                label.setVisible(false);
            cell.addChild(label);

        } else {
            var buildingType = category[this.indx][Number(strValue)].imageName;
            label = cell.getChildByTag(3);
            label.setString(category[this.indx][Number(strValue)].label);
            if (this.indx!=0) {
                if (baseGameBuildingInfo[buildingType] == undefined)
                    label.setString(category[this.indx][Number(strValue)].label+" undefined");
                label.setPosition(cc.p(winSize.width / 6, winSize.height * 27 / 40));
            }
            else{
                label.setPosition(cc.p(winSize.width/6,50));
            }


            sprite = cell.getChildByTag(2);
            sprite.setTexture(link.concat(category[this.indx][Number(strValue)].imageName).concat(".png"));
            if (this.indx!=0) {
                sprite.setScale(winSize.width / sprite.width / 3, winSize.height * 3 / sprite.height / 4);
            }
            else
                sprite.setScale(winSize.width / sprite.width / 3, winSize.height  / sprite.height / 2);

            sprite = cell.getChildByTag(1);
            if (this.indx!=0){
                sprite.setTexture("res/Arts/GUIs/shop_gui/slot.png");
                sprite.setScale(winSize.width/sprite.width/3,winSize.height * 3 / sprite.height / 4);
            }
            else{
                sprite.setTexture("res/Arts/GUIs/shop_gui/slot_catalogy.png");
                sprite.setScale(winSize.width/sprite.width/3,winSize.height/sprite.height/2);
            }

            sprite = cell.getChildByTag(4);
            if (this.indx!=0) {
                sprite.setTexture("res/Arts/GUIs/shop_gui/item_background.png");
                sprite.setScale(winSize.width/sprite.width/3,winSize.height * 3 / sprite.height / 4);
            }
            else {
                sprite.setTexture("res/Arts/GUIs/shop_gui/catalogy_bg.png");
                sprite.setScale(winSize.width/sprite.width/3,winSize.height/sprite.height/2);
            }

            sprite = cell.getChildByTag(6);
            if (this.indx!=0)
                sprite.setVisible(false);
            else
                sprite.setVisible(true);

            sprite = cell.getChildByTag(7);
            sprite.setTexture("res/Arts/GUIs/shop_gui/gold.png");
            var cost = 0;
            if (this.indx!=0) {
                sprite.setVisible(true);
                if (baseGameBuildingInfo[buildingType]!= undefined){
                    var coinNeed = baseGameBuildingInfo[buildingType]["1"]["coin"];
                    var goldNeed = baseGameBuildingInfo[buildingType]["1"]["gold"];
                    var elixirNeed = baseGameBuildingInfo[buildingType]["1"]["elixir"];

                    if (coinNeed == undefined)
                        coinNeed =0;
                    if (goldNeed == undefined)
                        goldNeed =0;
                    if (elixirNeed == undefined)
                        elixirNeed =0;
                    cost = goldNeed;
                    if (coinNeed!=0) {
                        cost = coinNeed;
                        sprite.setTexture("res/Arts/GUIs/shop_gui/g.png");
                    }
                    else if (elixirNeed!=0) {
                        sprite.setTexture("res/Arts/GUIs/shop_gui/elixir.png");
                        cost = elixirNeed;
                    }
                }
            }
            else
                sprite.setVisible(false);

            var label = cell.getChildByTag(8);
            label.setString(cost.toString());
            if (this.indx!=0) {
                if (this.isAbleToBuy(buildingType) != true)
                    label.setColor(cc.color(255, 32, 32));
                else
                    label.setColor(cc.color(255, 255, 255));
                label.setVisible(true);
            }
            else
                label.setVisible(false);




        }

        return cell;
    },

    numberOfCellsInTableView:function (table) {
        return category[this.indx].length;
    }
});

