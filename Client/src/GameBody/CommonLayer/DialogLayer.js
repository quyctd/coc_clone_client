/**
 * Created by CPU02302_LOCAL on 12/4/2018.
 */
var Dialog = cc.Layer.extend({

    ctor: function () {
        this._super();
        this.init();
    },

    init: function () {
        anim = new BaseBuilding();

        var winSize = cc.director.getWinSize();

        cc.spriteFrameCache.addSpriteFrames(res.UpgradeBuildingUI_small_plist);

        bg = new cc.Sprite("res/Arts/GUIs/nen.png");
        bg.setPosition(winSize.width / 2, winSize.height / 2);
        bg.scale = winSize.width / bg.width * 0.75;
        this.addChild(bg);

        //add building name
        infoName = new cc.LabelBMFont("This is Demo name", "res/Fonts/soji_24.fnt");
        infoName.setPosition(bg.x, bg.y + bg.height / 2 * bg.scale - infoName.height / 2 - 20);
        this.addChild(infoName);

        //Add x button
        var xBtn = ccui.Button();
        xBtn.loadTextures("res/Arts/GUIs/close.png", "res/Arts/GUIs/close.png", "res/Arts/GUIs/close.png");
        xBtn.scale = bg.scale;
        xBtn.setPosition(bg.x + bg.width / 2 * bg.scale - xBtn.width / 2 * xBtn.scale - 10, bg.y + bg.height / 2 * bg.scale - xBtn.height / 2 * xBtn.scale - 10);
        xBtn.addTouchEventListener(this.close_touchEvent, this);
        this.addChild(xBtn);

        //Add description
        var text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";
        descript = new cc.LabelBMFont(text, "res/Fonts/soji_20.fnt", winSize.width * 0.7, cc.TEXT_ALIGNMENT_CENTER);
        descript.setPosition(bg.x, bg.y - 10 - descript.height / 2);
        this.addChild(descript);

    },

    close_touchEvent: function (sender, type) {
        switch (type) {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;
            case ccui.Widget.TOUCH_ENDED:
                this.setVisible(false);
                gv.centralScene.resumeLayer();
                this.removeAllChildren();
                cc.spriteFrameCache.removeSpriteFramesFromFile(res.UpgradeBuildingUI_small_plist);
                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    changeContentByBuilding: function (building, type) {
        this.init();
        this.type = type;
        this.getInfoBuildingFromJson(building);
        this.addBuildingAnim(building);

        if (type === "info") {
            infoName.setString(gv.buildInfo[building.uid].name + " cấp " + building.level);
        }
        else if (type === "upgrade") {
            infoName.setString("Nâng lên cấp " + (building.level + 1));
            descript.setString("");
            this.addUpgradeTime(building);
            this.addButtonUpgrade();
        }

        var listInfo = this.judgeInfoByTypeBuilding(building.uid);
        var lenInfo = listInfo.length;
        for (var i = 1; i <= lenInfo; i++) {
            this.addInfoByType(listInfo[i - 1], i, lenInfo);
        }
    },

    addUpgradeTime: function (building) {
        upgradeText = cc.LabelBMFont("Thời gian nâng cấp: ", "res/Fonts/fista_24_non.fnt");
        upgradeText.color = new cc.Color(73, 58, 51);
        upgradeText.setPosition(anim.x, anim.y - anim.buildingSubstrateBottom.height / 2 * anim.scale);
        this.addChild(upgradeText, 2);

        var timeSec = baseGameBuildingInfo[building.uid][building.level + 1]["buildTime"];
        time = cc.LabelBMFont(Util.calculate_d_h_m_s(timeSec), "res/Fonts/soji_20.fnt");
        time.setPosition(anim.x, upgradeText.y - 20 - time.height / 2);
        this.addChild(time, 3);
    },

    addTextMaxLevel: function() {
        var textMax = cc.LabelBMFont("CHÚ Ý: Yêu cầu nhà chính cấp " + (gv.townHallLevel + 1), "res/Fonts/fista_20_non.fnt",  bg.width * 0.25, cc.TEXT_ALIGNMENT_CENTER);
        textMax.setName("textMax");
        textMax.color = new cc.Color(219, 10, 10);
        textMax.setPosition(upgradeBtn.x + upgradeBtn.width/2 * upgradeBtn.scale + 30 + textMax.width/2, upgradeBtn.y);
        this.addChild(textMax);
    },

    addButtonUpgrade: function () {
        var building = gv.centralScene.getChildByName("screen").m_building;
        var baseInfo = baseGameBuildingInfo[building.uid];
        var b_nextLevel = baseInfo[building.level + 1];

        upgradeBtn = ccui.Button();
        upgradeBtn.loadTextures("res/Arts/GUIs/button.png", "res/Arts/GUIs/button.png", "res/Arts/GUIs/button.png");
        upgradeBtn.scale = bg.scale;
        upgradeBtn.setPosition(bg.x, bg.y - bg.height / 2 * bg.scale + bg.height * bg.scale * 0.05 + upgradeBtn.height / 2 * upgradeBtn.scale);

        if (b_nextLevel["townHallLevelRequired"] > gv.townHallLevel) {
            upgradeBtn.setColor(cc.color(119, 118, 118));
            this.addTextMaxLevel();
        }
        else {
            upgradeBtn.addTouchEventListener(this.upgrade_touchEvent, this);
        }

        this.addChild(upgradeBtn, 2);

        var length = gv.upgradeCost.length;
        for (var i = 0; i < length; i++) {
            var lobbyNeeded = new cc.LabelBMFont(gv.upgradeCost[i], "res/Fonts/soji_16.fnt");
            this.addChild(lobbyNeeded, 2);

            var iconLobby = new cc.Sprite("#" + gv.upgradeType[i] + "_icon.png");
            this.addChild(iconLobby, 2);

            var index = Util.getIndexByLandI(length, i+1);
            lobbyNeeded.setPosition(upgradeBtn.x - 5 - iconLobby.width / 2, upgradeBtn.y + 20 * index);
            iconLobby.setPosition(lobbyNeeded.x + lobbyNeeded.width / 2 + 5 + iconLobby.width / 2, upgradeBtn.y + 20 * index);

            if (gv.upgradeCost[i] > gv.userData[gv.upgradeType[i]])
                lobbyNeeded.color = new cc.Color(229, 78, 61);
        }

    },

    upgrade_touchEvent: function (sender, type) {
        switch (type) {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;

            case ccui.Widget.TOUCH_ENDED:
                //Close screen
                this.close_touchEvent(sender, ccui.Widget.TOUCH_ENDED);

                var centralLayer = gv.centralScene.getChildByName("screen");
                var uiLayer = gv.centralScene.getChildByName("UILayer");
                if (gv.userData[LOBBY.BUILDER] > 0) {

                    var send = true;
                    for (var i = 0; i < gv.upgradeCost.length; i++) {
                        if (gv.userData[gv.upgradeType[i]] < gv.upgradeCost[i] || gv.userData[LOBBY.BUILDER] < 0)
                            send = false;
                    }
                    if (send) {
                        testnetwork.connector.sendMapupgradeItem(centralLayer.m_building.b_ID); //Ham tu goi update lobby
                    } else {

                        gv.centralScene.showNoti("lackLobby", -1);

                    }
                } else {

                    gv.centralScene.showNoti("maxBuilder", 100);

                }

                break;

            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    addBuildingAnim: function (building) {
        if (this.type === "info")
            var level = building.level;
        else if (this.type === "upgrade")
            level = building.level + 1;

        anim.init(building.uid, -1, building.buildSize, level, 0, -1);
        anim.scale = (bg.height * bg.scale / 4) / anim.buildingSubstrateBottom.height;
        this.addChild(anim, 1);
        // Anim chiếm 2/5 width, 1/4 height từ trên xuóng;
        anim.setPosition(bg.x - bg.width / 2 * bg.scale + bg.width / 5 * bg.scale, bg.y + bg.height / 2 * bg.scale - infoName.height - bg.height / 4 * bg.scale);
    },

    addInfoByType: function (type, index, length) {
        var str = type + "_Icon";
        this[str] = new cc.Sprite("#" + str + ".png");
        this.addChild(this[str]);

        this[type + "info_bar"] = new cc.Sprite("res/Arts/GUIs/info_bar.png");
        this[type + "info_bar_BG"] = new ccui.LoadingBar("res/Arts/GUIs/info_bar_BG.png");
        this[type + "info_bar_nextlv_BG"] = new ccui.LoadingBar("res/Arts/GUIs/info_bar_nextlv_BG.png");

        this.addChild(this[type + "info_bar"], 2);
        this.addChild(this[type + "info_bar_BG"], 4);
        this.addChild(this[type + "info_bar_nextlv_BG"], 3);

        this[type + "text"] = new cc.LabelBMFont("Null", "res/Fonts/soji_16.fnt");
        this.addChild(this[type + "text"], 5);

        this.judgePosByLength(type, index, length);
        this.setBarPercent(type);

    },

    judgeInfoByTypeBuilding: function (type) {
        switch (type) {
            case "RES_1":
                return ["Hitpoints", "Gold_ProductionRate", "Gold_Capacity"];
            case "TOW_1":
                return ["Hitpoints", "Elixir_Capacity", "Gold_Capacity"];
            case "BDH_1":
                return ["Hitpoints"];
            case "AMC_1":
                return ["Hitpoints", "TroopCapacity"];
            case "CLC_1":
                return ["Hitpoints", "TroopCapacity"];
            case "RES_2":
                return ["Hitpoints", "Elixir_ProductionRate", "Elixir_Capacity"];
            case "RES_3":
                return ["Hitpoints", "DarkElixir_ProductionRate", "DarkElixir_Capacity"];
            case "DEF_1":
                return ["Hitpoints", "Damage"];
            case "DEF_2":
                return ["Hitpoints", "Damage"];
            case "DEF_3":
                return ["Hitpoints", "Damage"];
            case "STO_1":
                return ["Hitpoints", "Gold_Capacity"];
            case "STO_2":
                return ["Hitpoints", "Elixir_Capacity"];
            case "STO_3":
                return ["Hitpoints", "DarkElixir_Capacity"];
            case "LAB_1":
                return ["Hitpoints"];
            case "BAR_1":
                return ["Hitpoints"];
            default:
                return ["Hitpoints"];
        }
    },

    getChildByType: function (type) {
        var icon = this[type + "_Icon"];
        var info_bar = this[type + "info_bar"];
        var info_bar_next = this[type + "info_bar_nextlv_BG"];
        var info_bar_bg = this[type + "info_bar_BG"];
        var text = this[type + "text"];

        return [icon, info_bar, info_bar_next, info_bar_bg, text];
    },

    judgePosByLength: function (type, index, length) {

        var icon, info_bar, info_bar_next, info_bar_bg, text;
        [icon, info_bar, info_bar_next, info_bar_bg, text] = this.getChildByType(type);

        var height_content_size = bg.height * bg.scale - infoName.height;
        var height_up = bg.y + bg.height / 2 * bg.scale - infoName.height;

        var start = Math.ceil(-length / 2);
        var paddingIndex = start + index - 1;
        if (length % 2 === 0 && paddingIndex >= 0)
            paddingIndex += 1;
        if (length % 2 === 0) {
            if (paddingIndex < 0) paddingIndex += 0.5;
            else paddingIndex -= 0.5;
        }

        icon.scale = bg.scale;
        info_bar.scale = bg.scale;
        info_bar_next.scale = bg.scale;
        info_bar_bg.scale = bg.scale;

        icon.setPositionX(bg.x - 50 - icon.width / 2 * icon.scale);
        icon.setPositionY(height_up - height_content_size / 4 + 20 * paddingIndex + info_bar.height * info_bar.scale * paddingIndex);

        info_bar.setPosition(bg.x + info_bar.width / 2 * info_bar.scale, icon.y);
        info_bar_next.setPosition(info_bar.x, info_bar.y);
        info_bar_bg.setPosition(info_bar.x, info_bar.y);

        text.setString(this.setStringByType(type));
        text.setPosition(info_bar.x - info_bar.width / 2 * info_bar.scale + info_bar.width / 6 * info_bar.scale + text.width / 2, info_bar.y);
    },

    setStringByType: function (type) {
        var str = type.toLowerCase();

        var retStr = "";
        if (str.indexOf("capacity") !== -1) {
            str = str.replace("_capacity", "");
            retStr = "Sức chứa: ";
            if (this.type === "info") {
                if (this.building.uid === "TOW_1")
                    retStr += gv.userData[str] + "/" + gv.userData["max" + Util.upperFirstChar(str)];
                else
                    retStr +=  "0/" + this.b_currentLevel["capacity"];
            } else {
                if (this.building.uid === "TOW_1")
                    retStr += this.b_currentLevel["capacity" + Util.upperFirstChar(str)] + " + " + (this.b_nextLevel["capacity" +  Util.upperFirstChar(str)] - this.b_currentLevel["capacity" +  Util.upperFirstChar(str)]);
                else
                    retStr += this.b_currentLevel["capacity"] + " + " + (this.b_nextLevel["capacity"] - this.b_currentLevel["capacity"]);
            }
            return retStr;
        }

        if (str.indexOf("hitpoints") !== -1) {
            retStr = "Máu: ";
            if (this.type === "info")
                retStr += this.b_currentLevel["hitpoints"];
            else
                retStr += this.b_currentLevel["hitpoints"] + " + " + (this.b_nextLevel["hitpoints"] - this.b_currentLevel["hitpoints"]);
            return retStr;
        }

        if (str.indexOf("product") !== -1) {
            retStr = "Sản lượng: ";
            if (this.type === "info")
                retStr += this.b_currentLevel["productivity"] + "/h";
            else
                retStr += this.b_currentLevel["productivity"] + " + " + (this.b_nextLevel["productivity"] - this.b_currentLevel["productivity"]) + "/h";
            return retStr;
        }

        if (str.indexOf("damage") !== -1) {
            retStr = "Sát thương: ";
            if (this.type === "info")
                retStr += this.b_currentLevel["damagePerSecond"];
            else
                retStr += this.b_currentLevel["damagePerSecond"] + " + " + (this.b_nextLevel["damagePerSecond"] - this.b_currentLevel["damagePerSecond"]);

            return retStr;
        }

    },

    setBarPercent: function (type) {

        var icon, info_bar, info_bar_next, info_bar_bg, text;
        [icon, info_bar, info_bar_next, info_bar_bg, text] = this.getChildByType(type);

        if (this.type === "info") {
            //info bar yellow - next
            info_bar_next.setPercent(0);

            //info bar green - current
            if (type.indexOf("Capacity") === -1 && type.indexOf("Damage") === -1)
                info_bar_bg.setPercent(100);
            else {
                if (this.building.uid === "TOW_1") {
                    var str = type.toLowerCase();
                    str = str.replace("_capacity", "");
                    info_bar_bg.setPercent(gv.userData[str] / gv.userData["max" + Util.upperFirstChar(str)] * 100);
                }
                else {
                    if (type.indexOf("Damage") !== -1 )
                        info_bar_bg.setPercent(this.b_currentLevel["damagePerSecond"]/ this.b_lastLevel["damagePerSecond"] * 100);
                    else
                        info_bar_bg.setPercent(0);
                }
            }
        }

        else if (this.type === "upgrade") {

            var key;
            if (this.building.uid !== "TOW_1" || type.indexOf("Capacity") === -1) {
                key = this.translateFromTypeToKey(type);
            }
            else {
                var str = type.toLowerCase();
                str = str.replace("_capacity", "");
                key = "capacity" + Util.upperFirstChar(str);
            }

            info_bar_bg.setPercent(this.b_currentLevel[key]/ this.b_lastLevel[key] * 100);
            info_bar_next.setPercent(this.b_nextLevel[key]/ this.b_lastLevel[key] * 100);
        }
    },

    translateFromTypeToKey : function(type) {
        switch (type) {
            case "Hitpoints":
                return "hitpoints";

            case "TroopCapacity":
                return "capacity";

            case "Gold_Capacity":
                return "capacity";

            case "Elixir_Capacity":
                return "capacity";

            case "DarkElixir_Capacity":
                return "capacity";

            case "Gold_ProductionRate":
                return "productivity";

            case "Elixir_ProductionRate":
                return "productivity";

            case "DarkElixir_ProductionRate":
                return "productivity";

            case "Damage":
                return "damagePerSecond";

            default:
                return "hitpoints";
        }
    },

    getInfoBuildingFromJson: function(building) {
        this.building = building;
        //Base info of building
        var baseInfo = baseGameBuildingInfo[building.uid];

        var lastLevel = Object.keys(baseInfo)[Object.keys(baseInfo).length - 1];

        this.b_lastLevel = baseInfo[lastLevel];
        this.b_currentLevel = baseInfo[building.level];
        if (building.level + 1 <= lastLevel)
            this.b_nextLevel = baseInfo[building.level + 1];
        else
            this.b_nextLevel = this.b_lastLevel;
    }

});