/**
 * Created by CPU02302_LOCAL on 12/4/2018.
 */
var Notification = cc.Layer.extend({

    ctor: function () {
        this._super();
    },

    init: function(type) {
        this.type = type;
        var winSize = cc.director.getWinSize();

        cc.spriteFrameCache.addSpriteFrames(res.UpgradeBuildingUI_plist);
        cc.spriteFrameCache.addSpriteFrames(res.Popup_plist);

        var bg = cc.Sprite("#BG.png");
        bg.setName("bg");
        bg.setPosition(winSize.width / 2, winSize.height / 2);
        bg.scale = winSize.width / bg.width * 0.5;
        this.addChild(bg);

        text = new cc.LabelBMFont("THÔNG BÁO", "res/Fonts/soji_24.fnt");
        text.setPosition(bg.x, bg.y + bg.height/2 * bg.scale - text.height/2 - 20);
        this.addChild(text);

        cancelBtn = ccui.Button();
        cancelBtn.loadTextures("button2.png", "button2.png", "button2.png", ccui.Widget.PLIST_TEXTURE);
        cancelBtn.setName("cancelBtn");
        cancelBtn.scale = 1.5;
        cancelBtn.setPosition(bg.x - bg.width/4 * bg.scale, bg.y - bg.height/2 * bg.scale + cancelBtn.height/2 * cancelBtn.scale + 50);
        cancelBtn.addTouchEventListener(this.close_touchEvent, this);
        this.addChild(cancelBtn, 2);

        cancelText = new cc.LabelBMFont("Hủy bỏ", "res/Fonts/soji_20.fnt");
        cancelText.setName("cancelText");
        cancelText.setPosition(cancelBtn.x, cancelBtn.y);
        this.addChild(cancelText, 3);

        okBtn = ccui.Button();
        okBtn.loadTextures("button.png", "button.png", "button.png", ccui.Widget.PLIST_TEXTURE);
        okBtn.scale = 1.5;
        okBtn.setPosition(bg.x + bg.width/4 * bg.scale, bg.y - bg.height/2 * bg.scale + okBtn.height/2 * okBtn.scale + 50);
        okBtn.addTouchEventListener(this.finish_touchEvent, this);
        this.addChild(okBtn, 2);

        okText = new cc.LabelBMFont("1000 G", "res/Fonts/soji_20.fnt");
        okText.setPosition(okBtn.x, okBtn.y);
        this.addChild(okText, 3);

        desc = new cc.LabelBMFont("Bạn có chắc muốn hoàn thành nhanh?", "res/Fonts/soji_20.fnt", bg.width * bg.scale * 0.7, cc.TEXT_ALIGNMENT_CENTER);
        desc.setPosition(bg.x, text.y - text.height/2 - 50 - desc.height/2 );
        this.addChild(desc, 3);

        cc.spriteFrameCache.removeSpriteFramesFromFile(res.Popup_plist);

        this.changeContentByActionType(this.type);
    },

    close_touchEvent: function(sender, type) {
        switch (type) {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;
            case ccui.Widget.TOUCH_ENDED:
                this.removeLackLobby();
                this.setVisible(false);
                gv.centralScene.resumeLayer();

                if (this.type === NOTI_STATUS.FULL_LOBBY) {
                    okBtn.setVisible(true);
                    okText.setVisible(true);
                }

                if (this.type === NOTI_STATUS.DISCONNECT) {
                   cc.director.runScene(new LoginScene());
                }

                break;
            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    finish_touchEvent: function(sender, type) {
        switch (type) {
            case ccui.Widget.TOUCH_BEGAN:
                break;

            case ccui.Widget.TOUCH_MOVED:
                break;
            case ccui.Widget.TOUCH_ENDED:

                this.removeLackLobby();

                if (this.value >= gv.userData[LOBBY.COIN]) //Không đủ coin thì báo
                    this.changeContentByActionType("lackCoin", this.value);

                if (this.type === NOTI_STATUS.QUICK_FINISH && this.value < gv.userData[LOBBY.COIN]) {
                    var building = fr.getCurrentScreen().m_building;
                    testnetwork.connector.sendQuickFinishBuilding(building.b_ID);
                    this.setVisible(false);
                    gv.centralScene.resumeLayer();
                }

                if (this.type === NOTI_STATUS.CANCEL_UPGRADE) {
                    var building = fr.getCurrentScreen().m_building;
                    testnetwork.connector.sendCancelUpgradeBuilding(building.b_ID);
                    this.setVisible(false);
                    gv.centralScene.resumeLayer();
                }

                if (this.type === NOTI_STATUS.LACK_LOBBY) {
                    var send = false;
                    for (var i = 0; i < gv.upgradeCost.length; i++) {
                        if (gv.upgradeCost[i] > gv.userData["max" + gv.upgradeType[i][0].toUpperCase() + gv.upgradeType[i].slice(1)])
                            send = true;
                    }

                    if (send) {
                        this.changeContentByActionType(NOTI_STATUS.FULL_LOBBY, this.value);
                    }
                    else {
                        //Do action buy lobby
                        for (var i = 0; i < gv.upgradeCost.length; i++) {
                            if (gv.userData[gv.upgradeType[i]] >= gv.upgradeCost[i])
                                continue;
                            testnetwork.connector.sendTradeResource(gv.upgradeCost[i] - gv.userData[gv.upgradeType[i]], TRADE_LOBBY[gv.upgradeType[i]]);
                        }
                        this.setVisible(false);
                        gv.centralScene.resumeLayer();
                        gv.centralScene.showSucess("Mua tài nguyên thành công!");
                        if (fr.getCurrentScreen().m_building.stateBuild !== BUILDING_STATUS.BUILDING_CREATE) {
                            gv.acceptUpgrade = true;
                            gv.centralScene.UILayer.resetBtn(fr.getCurrentScreen().m_building);
                        }
                        gv.acceptUpgrade = false;

                    }
                }

                if (this.type === NOTI_STATUS.MAX_BUILDER) {
                    testnetwork.connector.sendFreeTheMason();
                    this.setVisible(false);
                    gv.centralScene.resumeLayer();
                }

                break;
            case ccui.Widget.TOUCH_CANCELED:
                break;
        }
    },

    addLackLobby: function() {

        this.lackSprite = new cc.Sprite();

        for (var i = 0; i < gv.upgradeCost.length; i++) {

            if (gv.userData[gv.upgradeType[i]] >= gv.upgradeCost[i])
                continue;

            var lackCost = gv.upgradeCost[i] - gv.userData[gv.upgradeType[i]];

            var cost = cc.LabelBMFont(lackCost, "res/Fonts/soji_20.fnt");
            this.lackSprite.addChild(cost, 1);

            var icon = cc.Sprite("res/Arts/GUIs/"+ gv.upgradeType[i] +".png");

            cost.setPosition(this.lackSprite.x - 10, this.lackSprite.y - cost.height * i - 10 * i);
            icon.setPosition(cost.x + cost.width/2 + 10 + icon.width/2, cost.y);
            this.lackSprite.addChild(icon, 1);
        }

        this.lackSprite.setPosition(winSize.width/2, winSize.height/2);
        this.addChild(this.lackSprite);

    },

    removeLackLobby: function() {
        if (this.lackSprite)
            this.lackSprite.setVisible(false);
    },

    changeContentByActionType: function(action, value) {
        this.type = action;
        var bg = this.getChildByName("bg");

        this.getChildByName("cancelBtn").x = bg.x - bg.width/4 * bg.scale;
        this.getChildByName("cancelText").x = bg.x - bg.width/4 * bg.scale;
        this.value = value;

        if (action === NOTI_STATUS.QUICK_FINISH) {
            text.setString("THÔNG BÁO");
            desc.setString("Bạn có chắc muốn hoàn thành nhanh?");
            cancelText.setString("Hủy bỏ");
            okText.setString(value + " G");
        }

        else if (action === NOTI_STATUS.CANCEL_UPGRADE) {
            text.setString("Hủy nâng cấp?");
            desc.setString("Bạn có muốn hủy nâng cấp? Bạn sẽ chỉ nhận được một nửa số tiền.");
            cancelText.setString("Đóng");

            okText.setString("Đồng ý");
        }

        else if (action === NOTI_STATUS.MAX_BUILDER) {
            text.setString("THỢ XÂY BẠN ĐANG HẾT");
            desc.setString("Giải phóng một thợ xây.");
            cancelText.setString("Hủy bỏ");

            okText.setString("Giải phóng");
        }

        else if (action === NOTI_STATUS.LACK_COIN) {
            text.setString("THIẾU G");
            desc.setString("Bạn không đủ G.");
            cancelText.setString("Hủy bỏ");
            okText.setString( "Nạp 1000 G");
        }

        else if (action === NOTI_STATUS.LACK_LOBBY) {
            text.setString("THIẾU TÀI NGUYÊN");
            desc.setString("Bạn có muốn mua số tài nguyên còn thiếu");
            cancelText.setString("Hủy bỏ");
            this.addLackLobby();

            var coinNeeded = 0;
            for (var i = 0; i< gv.upgradeCost.length; i++) {
                if (gv.userData[gv.upgradeType[i]] >= gv.upgradeCost[i])
                    continue;
                coinNeeded += Util.convertLobbyTocoin(gv.upgradeType[i], gv.upgradeCost[i] - gv.userData[gv.upgradeType[i]]);
            }
            okText.setString( coinNeeded + " G");
        }

        else if (action === NOTI_STATUS.FULL_LOBBY) {
            text.setString("THIẾU TÀI NGUYÊN");
            desc.setString("Bạn cần mở rộng nhà kho");
            cancelText.setString("Hủy bỏ");
            this.getChildByName("cancelBtn").x = bg.x;
            this.getChildByName("cancelText").x = bg.x;
            okBtn.setVisible(false);
            okText.setVisible(false);
        }

        else if (action === NOTI_STATUS.DISCONNECT) {
            text.setString("MẤT KẾT NỐI");
            desc.setString("Vui lòng kết nối lại.");
            cancelText.setString("Kết nối lại");
            this.getChildByName("cancelBtn").x = bg.x;
            this.getChildByName("cancelText").x = bg.x;
            okBtn.setVisible(false);
            okText.setVisible(false);
        }
    }
});