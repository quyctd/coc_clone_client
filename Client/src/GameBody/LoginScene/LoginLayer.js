/**
 * Created by CPU02302_LOCAL on 12/4/2018.
 */


var LoginLayer = cc.Layer.extend({
        winSize: null,
        uiTextField: null,
        ctor: function () {
            this._super();
            this.init();
        }
        ,

        init: function () {
            var self = this;
            this.flag = false;
            this.winSize = cc.director.getWinSize();
            var winSize = this.winSize;

            if (Util.milisecondNow() % 2 != 0) {
                var bg = cc.Sprite(res.bg_login_2);
            } else {
                var bg = cc.Sprite(res.bg_login_3);
            }

            bg.setScaleX(winSize.width / bg.width);
            bg.setScaleY(winSize.height / bg.height);

            bg.setPosition(cc.p(winSize.width / 2, winSize.height / 2));
            this.addChild(bg);


            //--------------------------


            var text_err = new cc.LabelBMFont("", "res/Fonts/soji_24.fnt");
            this.text_err = text_err;
            text_err.setPosition(winSize.width / 2, winSize.height / 2.6);

            this.addChild(text_err, 1);
            //--------------------------


            //
            var sefl = this;
            var cheat = new ccui.Button();
            cheat.loadTextures("res/Arts/GUIs/button.png", "res/Arts/GUIs/button.png", "res/Arts/GUIs/button.png");
            // cheat.setString("Cheat");
            cheat.scale = 1.5;
            cheat.addTouchEventListener(function (sender, type) {
                switch (type) {
                    case ccui.Widget.TOUCH_BEGAN:
                        cc.log("Touch down");
                        break;

                    case ccui.Widget.TOUCH_MOVED:
                        cc.log("Touch moved");
                        break;
                    case ccui.Widget.TOUCH_ENDED:
                        cc.log("Calling ");
                        var text = this.uiTextField.getString();
                        if (text == null || text == "") {
                            text_err.setString("Vui lòng nhập tên Vương Quốc của bạn!");
                        } else {
                            self.onSelectLogin();
                            text_err.setString("");

                            sefl.schedule(this.scoreCounter, 0.01);
                        }

                        break;
                    case ccui.Widget.TOUCH_CANCELED:
                        cc.log("Touch canceled");
                        break;
                }
            }, this);
            cheat.x = winSize.width * 0.7;
            cheat.y = winSize.height * 0.3;
            cheat.setName("Cheat");
            this.addChild(cheat, 1);

            var cheatText = new cc.LabelBMFont("LOGIN", "res/Fonts/soji_16.fnt");
            cheatText.setPosition(cheat.x, cheat.y);
            this.addChild(cheatText, 2);


//----------------------------------------------------

            var bg_text = cc.Sprite(res.g_background);
            bg_text.x = winSize.width * 0.5;
            bg_text.y = winSize.height * 0.3;
            bg_text.setScaleX(3);
            bg_text.setScaleY(2.5);
            this.addChild(bg_text)


            var logo = cc.Sprite(res.bg_logo);
            logo.x = winSize.width * 0.1;
            logo.y = winSize.height * 0.9;
            logo.setScale(2);
            this.addChild(logo)

            this.uiTextField = new ccui.TextField("Tên vương quốc");
            var uiTextField = this.uiTextField;

            uiTextField.x = winSize.width * 0.5;
            uiTextField.y = winSize.height * 0.3;
            uiTextField.setMaxLength(15);
            uiTextField.setMaxLengthEnabled(true);
            uiTextField.setScale(1.6);

            this.addChild(uiTextField);


            this.connectText = new cc.LabelBMFont("Connecting...", "res/Fonts/soji_24.fnt");
            var connectText = this.connectText;

            connectText.x = winSize.width * 0.5;
            connectText.y = winSize.height * 0.1;




//----------------------------------------------------


            this.trainBar = new ccui.LoadingBar();
            this.trainBar.setName("TrainBar");
            //   this.trainBar.scale= 0.5;
            this.trainBar.setScaleX(2);
            this.trainBar.setScaleY(2);

            this.trainBar.loadTexture(res.g_loading);

            this.trainBar.x = winSize.width * 0.5;
            this.trainBar.y = winSize.height * 0.1;
            this.time = 0;
            this.trainBar.setPercent(this.time);

            this.trainBar.setOpacity(200);
            this.addChild(this.trainBar, zorder.z_anim + 2);
            this.bg_load = cc.Sprite(res.bg_loading);
            var bg_load = this.bg_load;
            bg_load.x = winSize.width * 0.5;
            bg_load.y = winSize.height * 0.1;
            bg_load.setScaleX(2);
            bg_load.setScaleY(2);
            bg_load.setOpacity(100);
            this.addChild(bg_load);
            this.key = true;
            this.connectText.setVisible(false);
            this.addChild(this.connectText);
            return true;
        },
        scoreCounter: function () {
            if (this.time == 100) {

                this.trainBar.setVisible(false);
                this.bg_load.setVisible(false);
                this.connectText.setVisible(true);
                if (this.flag) {
                     this.time++;
                }
                else{
                    this.time=0;
                }

            }
            if(this.time==106){
                this.nextScene();
            }
            this.time = this.time + 1;

            this.trainBar.setPercent(this.time);
        }

        ,
        onSelectLogin: function (sender) {

            gv.gameClient.connect();
            ;

        }
        ,
        onConnectSuccess: function () {
            cc.log("CONNECT SUCCESS");
            this.key = true;
        }
        ,
        onConnectFail: function (text) {
            this.time = 0;
            this.key = false;
            this.text_err.setString("Kết nối không thành công!");
            this.connectText.setString("");
        }
        ,
        onFinishLogin: function () {
            this.flag = true;


        }
        ,
        onUserInfo: function (userName) {
            this._gameNode.setVisible(true);

        }
        ,
        updateMove: function (x, y) {

        }
        ,
        onUserRes: function (userName) {


        }
        ,
        nextScene: function () {
            SceneManager.runScene(SCENE_CENTRAL);
        },
        getTextInput: function () {
            cc.log("GET INPUT");
            var text = this.uiTextField.getString();
            if (text == null || text == "") return "";

            return text;
        },
        onDisconnect: function () {
            this.text_err.setString("Mất kết nối");
        }

    })
    ;
