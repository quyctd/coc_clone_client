/**
 * Created by CPU02302_LOCAL on 12/4/2018.
 */

//Init UserData -> Check exist User -> Get UserData -> Get Login Layer

var LoginScene = cc.Scene.extend({
    loginLayer: null,

    ctor: function() {
        this._super();
        this.init();
    },

    init: function() {
        this.loginLayer = new LoginLayer();
        this.loginLayer.setName("screen");

        this.addChild(this.loginLayer, 1);

        //this.setScale(this.scaleSize);

        return this;
    }

});
