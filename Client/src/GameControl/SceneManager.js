/**
 * Created by CPU02302_LOCAL on 12/4/2018.
 */

var SceneManager = cc.Class.extend({

    centralScene: false,
    loginScene: false


});

SceneManager.runScene = function(sceneID) {

    var scene = null;

    switch (sceneID){
        case "SCENE_LOGIN":
            scene =  new LoginScene();
            break;
        case "SCENE_CENTRAL":
            scene = new CentralScene();
            gv.centralScene = scene;
            break;
        default:
            scene = new LoginScene();
    }

    cc.director.runScene(scene);

};
