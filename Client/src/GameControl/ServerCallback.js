var ServerCallback = cc.Class.extend({

    callUpgradeBuilding: function (id, timeLeft) {
        // cc.log("Server callback: upgrade item");
        if(!gv["centralScene"]) return;
        var building = gv.centralScene.getChildByName("screen").getBuildingById(id);
        var uiLayer = gv.centralScene.getChildByName("UILayer");
        uiLayer.InfoBtnDisappear(building);
        building.stateBuild = BUILDING_STATUS.BUILDING_BUILDING;
        uiLayer.InfoBtnAppear(building);
        building.upgradeBuilding(timeLeft);

    },

    callUpgradeBuildingFinish: function (id, level) {
        // cc.log("Server callback: upgrade item finish");
        if(!gv["centralScene"]) return;
        var building = gv.centralScene.getChildByName("screen").getBuildingById(id);
        building.drawLevel(level);
    },

    callRemoveObs: function (id, timeLeft) {
        // cc.log("Server callback: remove obs");
        if(!gv["centralScene"]) return;
        var obs = gv.centralScene.getChildByName("screen").getBuildingById(id);
        gv.centralScene.UILayer.InfoBtnDisappear(obs);
        obs.removeObs(timeLeft);
        gv.centralScene.UILayer.InfoBtnAppear(obs);

    },

    callRemoveObsFinish: function (id) {
        // cc.log("Server callback: remove obs");
        if(!gv["centralScene"]) return;
        var obs = gv.centralScene.getChildByName("screen").getBuildingById(id);
        obs.removeFinish();
    },

    callUpgradeLobby: function (gold, elixir,darkElixir, coin,  builder, level, trophy, army) {
        // cc.log("callUpgradeLobby");
        if(!gv["centralScene"]) return;
        var lobby = gv.centralScene.getChildByName("lobby");

        gv.userData[LOBBY.GOLD] = gold;
        gv.userData[LOBBY.ELIXIR] = elixir;
        gv.userData[LOBBY.COIN] = coin;
        gv.userData[LOBBY.DARK_ELIXIR] = darkElixir;

        gv.userData[LOBBY.BUILDER] = builder;
        gv.userData[LOBBY.LEVEL] = level;
        gv.userData[LOBBY.TROPHY] = trophy;
        gv.userData[LOBBY.ARMY] = army;


        lobby.update(LOBBY.ALL);
    },
    callUpgradeLobbyMAX: function (gold, elixir,darkElixir, coin,   builder, level, trophy, army) {
        // Util.log("callUpgradeLobbyMAX");
        if(!gv.centralScene) return;
        var lobby = gv.centralScene.getChildByName("lobby");

        gv.userData[LOBBY.MAX_GOLD] = gold;
        gv.userData[LOBBY.MAX_ELIXIR] = elixir;
        gv.userData[LOBBY.MAX_COIN] = coin;
        gv.userData[LOBBY.MAX_DARK_ELIXIR] = darkElixir;

        gv.userData[LOBBY.MAX_BUILDER] = builder;
        //gv.userData[LOBBY.LEVEL] = level;
       // gv.userData[LOBBY.TROPHY] = trophy;
        gv.userData[LOBBY.MAX_ARMY] = army;


        lobby.update(LOBBY.ALL);
    },

    callCreateBuilding: function (id, type, posx, posy, timeLeft) {
        // cc.log("Server callback: create item");
        if(!gv["centralScene"]) return;
        var shopLayer = gv.centralScene.getChildByName("Shop");
        shopLayer.addBuildingFromShop(id, type, posx, posy, timeLeft);
        // shopLayer.setVisible(false);
        gv.centralScene.removeChildByName("Shop");
        gv.centralScene.resumeLayer();

    },

    callCreateBuildingFinish: function () {
        // cc.log("Server callback: create item finish");

    },

    callMoveItem: function (id, x, y) {
        // cc.log("Server callback: Move item");
        gv.centralScene.getChildByName("screen").putBuildingDown(id, x, y);
    },

    callErrString: function(str) {
        // cc.log("Server callback: Error", str);
        gv.centralScene.showError(str);
    }

});

var serverCallback = new ServerCallback();