/**
 * Created by CPU02302_LOCAL on 12/12/2018.
 */

var GENDER = {
    GENDER_MALE: 0,
    GENDER_FEMALE: 1
};

//Loại công trình
var BUILDING_TYPE = {
    BUILDINGTYPE_NOGAIN : 0,            //Nhà cơ bản
    BUILDINGTYPE_ARMY : 1,              //Quân đội
    BUILDINGTYPE_TOWNHALL : 100,        //Nhà chính
    BUILDINGTYPE_RESOURCE : 200,        // Nhà chứa tài nguyên
    BUILDINGTYPE_DEFENSE : 300,         // nhà phòng thủ
    BUILDINGTYPE_WALL : 400,            // tường
    BUILDINGTYPE_BUILDER : 500           // nhà builder
};

//Loại nhà quân đội
var ARMY_TYPE = {
    ARMY_TROOPHOUSING : 1,              //Nhà chứa quân
    ARMY_BARRACK : 2,                   //Nhà xây quân
    ARMY_LABORATORY : 3                //Nhà nâng cấp quân
};

//Loại nhà phòng thủ
var DEFENSE_TYPE = {
    DEFENSE_CANNON : 300,               //Canon
    DEFENSE_ARCHERTOWER : 301,          //Archer
    DEFENSE_WIZARDTOWER : 302,          //Wizard
    DEFENSE_AIRDEFENSE : 303,           //Air defense
    DEFENSE_MORTAR : 304,               //mortar
    DEFENSE_TESLATOWER : 305            //Tesla
};

//Loại nhà resource
var RESOURCE_TYPE = {
    RESOURCE_ELIXIRPUMP : 200,          //Máy đẻ elixir
    RESOURCE_ELIXIRSTORAGE : 201,       //Kho chứa elixir
    RESOURCE_GOLDMINE : 202,            //Máy đẻ vàng
    RESOURCE_GOLDSTORAGE : 203         //Kho chứa vàng
};

//Trạng thái của nhà
var BUILDING_STATUS = {
    BUILDING_FREE : 0,      //Rảnh
    BUILDING_BUILDING : 1,  //Đang xây
    BUILDING_CREATE: 2 //Khởi tạo
};

//Dữ liệu người dùng cần lưu
var userData = cc.Class.extend({
    user_id: 1,
    used_language: null,
    is_opensound: false,
    volume_bgmusic : 0,
    volume_effectsound: 0,
    gold: null,        //gold
    elixir : null,     //elixir
    coin : null,
    darkElixir: null,
    builderNumber: 0
});

var BaseBuildingLevelData = cc.Class.extend({
    id: null,  //id
    build_level: null, //level xây dựng
    building_TypeID: null, //loại nhà được xây
    name: null, //tên nhà
    exportName: null, //tên nhà trong json
    BuildTimeD: null, // thời gian xây Day
    BuildTimeH: null, // thời gian xây Hour
    BuildTimeM: null, // thời gian xây Minute
    BuildCost: null, // Số tiền cần để xây
    TownHallLevel: null, //Cấp nhà chính để có thể xây
    MaxStoredGold: null, // Max lưu trữ gold
    MaxStoredElixir: null, // Max lưu trữ elixir
    HousingSpace: null, // số builder cần để xây
    ResourcePerHour: null, // số resource trả về
    ResourceMax: null, // Max resource thu được
    UnitProduction: null, //
    Hitpoints: null, // máu
    Damage: null, //dame
    Projectile: null, // đạn
    DefenderCharacter: null, // loại lính đứng trên tháp thủ
    DefenderCount: null, // số lính đứng trên tháp
    DestructionXP: null // kinh nghiệm thu được sau khi phá hủy
});

var BaseBuildingTypeData = cc.Class.extend({
    id: null, //id
    name: null, // tên nhà
    building_TypeID: null, //loại nhà ID
    BuildResource: null, //loại tài nguyên để build
    Width: null, // độ rộng
    Height: null, // độ dài
    ExportNameBuildAnim: null, // tên anim
    Bunker: null, // có phải pháo đài hay ko
    ProducesResource: null, // loại tài nguyên sản xuất
    UpgradesUnits: null, // có thể nâng cấp lính hay ko?
    AttackRange: null, // phạm vi tấn công
    AttackSpeed: null, // tốc độ tấn công
    BuildingW: null, // phạm vi va chạm của building: width
    BuildingH: null, // phạm vi va chạm của building: height
    AirTargets: null, // có tấn công trên không không
    GroundTargets: null, // có tấn công dưới đất không
    MinAttackRange: null, // vùng bị mù
    DamageRadius: null, // phạm vi tấn công của chưởng bắn za
    Locked: null, // Cần mở khóa
    Hidden: null, // có ẩn ko
    TriggerRadius: null // phạm vi kích hoạt

});

var LOBBY = {
    GOLD: "gold",
    MAX_GOLD: "maxGold",
    ELIXIR: "elixir",
    MAX_ELIXIR: "maxElixir",
    COIN: "coin",
    MAX_COIN: "maxCoin",
    DARK_ELIXIR : "darkElixir",
    MAX_DARK_ELIXIR: "maxDarkElixir",
    ARMY : "army",
    MAX_ARMY: "maxArmy",
    BUILDER : "builderNumber",
    MAX_BUILDER : "maxBuilderNumber",
    TROPHY : "trophy",
    EXP : "exp",
    LEVEL : "level",
    ALL:"all",
    goldRate: 100,
    elixirRate: 100,
    darkElixirRate: 30

};

var TRADE_LOBBY = {
  gold: 1,
  elixir: 2,
  darkElixir: 3
};

var NOTI_STATUS = {
  QUICK_FINISH: "quick_finish",
  CANCEL_UPGRADE : "cancel",
  MAX_BUILDER: "maxBuilder",
    LACK_COIN: "lackCoin",
    LACK_LOBBY: "lackLobby",
    FULL_LOBBY : "fullLobby",
    DISCONNECT: "disconnect"

};