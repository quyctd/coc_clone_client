/**
 * Created by CPU02302_LOCAL on 12/6/2018.
 */

var res = {
    //Map res
    map_tmx: "res/Arts/Map/42x42map.tmx",
    mapGrass_plist: "res/Arts/Map/mapGrass.plist",
    mapGrass_png: "res/Arts/Map/mapGrass.png",
    mapLayer_plist: "res/Arts/Map/mapLayer.plist",
    mapLayer_png: "res/Arts/Map/mapLayer.png",
    mapMoveUtil_plist: "res/Arts/Map/mapMoveUtil.plist",
    mapMoveUtil_png: "res/Arts/Map/mapMoveUtil.png",
    mapObjBg_plist: "res/Arts/Map/mapObjBg.plist",
    mapObjBg_png: "res/Arts/Map/mapObjBg.png",

    //defense res
    defenseBase_plist: "res/Arts/Building/DefenseBase/defense.plist",
    defenseBase_png: "res/Arts/Building/DefenseBase/defense.png",

    //UI res
    mainGUI_plist: "res/Arts/GUIs/mainGUI.plist",
    mainGUI_png: "res/Arts/GUIs/mainGUI.png",
    ActionBuilding_png: "res/Arts/GUIs/ActionBuilding.png",
    ActionBuilding_plist: "res/Arts/GUIs/ActionBuilding.plist",
    UpgradeBuildingUI_plist: "res/Arts/GUIs/upgrade_building_gui.plist",
    UpgradeBuildingUI_png: "res/Arts/GUIs/upgrade_building_gui.png",
    UpgradeBuildingUI_small_plist: "res/Arts/GUIs/upgrade_building_gui_small.plist",
    UpgradeBuildingUI_small_png: "res/Arts/GUIs/upgrade_building_gui_small.png",
    TrainTroopUI_plist: "res/Arts/GUIs/train_troop_gui.plist",
    TrainTroopUI_png: "res/Arts/GUIs/train_troop_gui.png",
    Popup_plist: "res/Arts/GUIs/popup.plist",
    Popup_png: "res/Arts/GUIs/popup.png",

    //login
    button_login_png: "res/Arts/Login/okbutton.png",
    button_login_selected_png: "res/Arts/Login/okbutton.png",
    button_login_disable_png: "res/Arts/Login/okbutton.png",
    bg_login_jpg: "res/Arts/Login/bg.jpg",
    bg_login_gif: "res/Arts/Login/login_gif.gif",
    bg_login_2: "res/Arts/Login/2.jpg",
    bg_login_3: "res/Arts/Login/3.jpg",
    bg_loading: "res/Arts/Login/loading.jpg",
    bg_logo: "res/Arts/Login/logo.png",
    g_background: "res/Arts/Login/g_background.png",
    g_loading: "res/Arts/Login/train_bar.png",
    bg_loading: "res/Arts/Login/bg_train_bar.png",


    //Config file res
    jsArmyCamp: "res/ConfigJson/ArmyCamp.json",
    jsBarrack: "res/ConfigJson/Barrack.json",
    jsBuilderHut: "res/ConfigJson/BuilderHut.json",
    jsClanCastle: "res/ConfigJson/ClanCastle.json",
    jsDefence: "res/ConfigJson/Defence.json",
    jsLaboratory: "res/ConfigJson/Laboratory.json",
    jsObstacle: "res/ConfigJson/Obstacle.json",
    jsResource: "res/ConfigJson/Resource.json",
    jsStorage: "res/ConfigJson/Storage.json",
    jsTownHall: "res/ConfigJson/TownHall.json",
    jsTroop: "res/ConfigJson/Troop.json",
    jsTroopBase: "res/ConfigJson/TroopBase.json",
    jsWall: "res/ConfigJson/Wall.json",

    //Init game data
    initGameData: "res/ConfigJson/InitGame.json",
    buildInfoData: "res/ConfigJson/BuildingInfoConfig.json",
    obsInfoData: "res/ConfigJson/ObstacleInfoConfig.json"



};

var gameRes = {
  //uiBtnConfig
    uiBtnConfig: "res/GameConfig/uiBtnConfig.json",
    uiTranslater: "res/GameConfig/uiTranslater.json",

    //music
    bgMusic_mp3: "res/Sounds/theme.mp3",
    bgMusic_ogg: "res/Sounds/theme.ogg",

    button_click: "res/Sounds/sfx/button_click.mp3",
    building_contruct: "res/Sounds/sfx/building_contruct.mp3",
    building_finish: "res/Sounds/sfx/building_finish.mp3",
    building_destroyed: "res/Sounds/sfx/building_destroyed.mp3"
};

