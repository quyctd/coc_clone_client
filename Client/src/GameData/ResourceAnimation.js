/*
	Auto generate
*/

//id
var resAni = {
    RES_1: "res/Arts/Effects/GoldMine/",
    RES_2: "res/Arts/Effects/ElixirCollector/",
    AMC_1: "res/Arts/Effects/ArmyCamp/",
    LAB_1: "res/Arts/Effects/Laboratory/"

};

//path
var resIdle = {
    //build
    BDH_1: "res/Arts/Building/BuilderHut/",
    AMC_1: "res/Arts/Building/ArmyCamp/",
    RES_1: "res/Arts/Building/GoldMine/",
    RES_2: "res/Arts/Building/ElixirCollector/",
    RES_3: "res/Arts/Building/DarkElixirCollector/",
    TOW_1: "res/Arts/Building/Townhall/",
    CLC_1: "res/Arts/Building/ClanCastle/",
    BAR_1 : "res/Arts/Building/Barrack/",
    DEF_1 : "res/Arts/Building/Cannon/",
    DEF_2 : "res/Arts/Building/AcherTower/",
    DEF_3 : "res/Arts/Building/Motar/",
    LAB_1 : "res/Arts/Building/Labratory/",
    STO_1: "res/Arts/Building/GoldStorage/",
    STO_2: "res/Arts/Building/ElixirStorage/",
    STO_3: "res/Arts/Building/DarkElixirStorage/",
    WAL_1: "res/Arts/Building/Wall/",

    //OBS
    OBS: "res/Arts/Building/Obstacle/"
};