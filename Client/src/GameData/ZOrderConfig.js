
var zorder = {
    z_map: 0,
    z_background: 0,

    z_building: 1,
    z_obs: 1,

    z_substrate: 2,
    z_greenSubstrate: 3,
    z_redSubstrate: 3,
    z_shadow: 4,
    z_idle:5,
    z_anim: 6,
    z_arrow: 7,
    z_labelName: 7,

    z_loading: 8,
    z_trainBar: 9,


    //float
    z_click_float: 100,

    //Lobby layer
    z_lobby: 1000
};