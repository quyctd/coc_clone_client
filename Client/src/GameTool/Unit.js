
var Unit = cc.Sprite.extend({

    sprite: null,
    shadow_sprite: null,
    ani_state: null,
    action_restart: null,

    ctor: function() {
        this._super();
    },

    set_ani_state: function(state) { //set animation

        if (this.ani_state) {
            this.ani_state.removeAllChildrenWithCleanup(true);
            this.ani_state = null;
        }
        if (state) {
            this.ani_state = state;
        }
    },

    set_action: function(action) {
        if (this.ani_state.isHaveAction(action)){
            this.ani_state.setAction(action);
            this.action_restart = true;
            return true;
        }
        return false;
    }
});
