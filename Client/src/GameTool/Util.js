/**
 * Created by CPU02302_LOCAL on 12/12/2018.
 */

var Util = cc.Class.extend({

    mul: function (p1, p2, p0) {
        return ((p1.x - p0.x) * (p2.y - p0.y) - (p2.x - p0.x) * (p1.y - p0.y));
    },

    inside: function (tr, p) {
        var d1, d2, d3;
        var has_neg, has_pos;

        d1 = this.mul(p, tr[0], tr[1]);
        d2 = this.mul(p, tr[1], tr[2]);
        d3 = this.mul(p, tr[2], tr[0]);

        has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
        has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

        return !(has_neg && has_pos);
    },

    milisecondNow: function () {
        var now;
        now = (new Date()).getTime();
        return (Math.ceil(now / 1000));
    },
    getCurentTime: function () {
        var time = new Date().getTime();
        var date = new Date(time);
        var s = date.toString();
        return s;
    },
    log: function (TAG, content) {


        cc.log("" + " " + TAG + " " + content);
    },
    parseInt: function (id) {


        return parseInt(id);
    },

    calculate_d_h_m_s: function (time) {
        var d, h, m, s;

        if (time !== 0) {
            s = time % 60;
            time = Math.floor(time / 60);

            m = time % 60;
            time = Math.floor(time / 60);

            h = time % 24;
            time = Math.floor(time / 24);

            d = time;
        }
        else return "";

        var strTime;

        if (d !== 0) {
            if (h === 0) {
                strTime = d.toString() + "d";
            }
            else {
                strTime = d.toString() + "d" + h.toString() + "h";
            }

        }
        else if (h !== 0) {
            if (m === 0) {
                strTime = h.toString() + "h";
            }
            else {
                strTime = h.toString() + "h" + m.toString() + "m";
            }
        }
        else if (m !== 0) {
            if (s === 0) {
                strTime = m.toString() + "m";
            }
            else {
                strTime = m.toString() + "m" + s.toString() + "s";
            }
        }
        else if (s !== 0) {
            strTime = s.toString() + "s";
        }
        else {
            strTime = "";
        }

        return strTime;
    },

    timeToCoin: function (timeLeft) {
        if (timeLeft <= 0)
            return 0;

        var coin = Math.ceil(timeLeft / 60) * 2;
        return coin;
    },

    convertLobbyTocoin: function(type, cost) {
        var ret = 0;
        ret = cost/LOBBY[type+"Rate"];
        return Math.ceil(ret);
    },

    toString: function(number) {

    },

    getIndexByLandI: function(index, length) {
        var start = Math.ceil(-length / 2);
        var i = start + index - 1;

        if (length % 2 === 0) {
            if (i < 0) i += 0.5;
            else i -= 0.5;
        }
        return i;
    },

    getValue: function(variable) {
        return JSON.parse(JSON.stringify(variable));
    },

    upperFirstChar: function(text) {
        var ret = text.charAt(0).toUpperCase() + text.slice(1);
        return ret;
    }

});

var Util = new Util();
