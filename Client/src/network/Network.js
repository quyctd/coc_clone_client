/**
 * Created by KienVN on 10/2/2017.
 */

var gv = gv || {};
var testnetwork = testnetwork || {};

testnetwork.Connector = cc.Class.extend({
    ctor: function (gameClient) {
        this.gameClient = gameClient;
        gameClient.packetFactory.addPacketMap(testnetwork.packetMap);
        gameClient.receivePacketSignal.add(this.onReceivedPacket, this);
        this._userName = "username";
    },
    onReceivedPacket: function (cmd, packet) {
        cc.log("onReceivedPacket:", cmd);

        switch (cmd) {
            //USER 1000-----------------------------------------------------------------------------------------------
            case gv.CMD.HAND_SHAKE:
                this.sendLoginRequest(fr.getCurrentScreen().getTextInput());
                break;
            case gv.CMD.USER_LOGIN:
                cc.log("Login success");
                this.sendGetUserInfo();
                break;
            case gv.CMD.USER_INFO:
                fr.getCurrentScreen().onUserRes(packet.data.coin);
                gv.userData = packet.data;
                this.sendGetMap();
                break;
            case gv.CMD.MOVE:
                cc.log("MOVE:", packet.x, packet.y);
                fr.getCurrentScreen().updateMove(packet.x, packet.y);
                break;
            case gv.CMD.USER_RES:
                fr.getCurrentScreen().onUserRes(packet.data.coin);
                this.sendGetMap();
                break;
            case gv.CMD.USER_UPDATE_LOBBY:

                Util.log("USER_UPDATE_LO    BBY", packet.gold + " " +
                    packet.elixir + " " +

                    packet.darkElixir + " " +
                    packet.coin + " " +
                    packet.builder + " " +
                    packet.level + " " +
                    packet.trophy + " " +
                    packet.army);


                serverCallback.callUpgradeLobby(packet.gold,
                    packet.elixir,

                    packet.darkElixir,
                    packet.coin,
                    packet.builder,
                    packet.level,
                    packet.trophy,
                    packet.army);

                break;


            case gv.CMD.USER_UPDATE_LOBBY_MAX:

                Util.log("USER_UPDATE_LOBBY_MAX", packet.gold + " " +
                    packet.elixir + " " +

                    packet.darkElixir + " " +
                    packet.coin + " " +
                    packet.builder + " " +
                    packet.level + " " +
                    packet.trophy + " " +
                    packet.army);
                serverCallback.callUpgradeLobbyMAX(packet.gold,
                    packet.elixir,

                    packet.darkElixir,
                    packet.coin,
                    packet.builder,
                    packet.level,
                    packet.trophy,
                    packet.army);
                break;


            //MAP 3000--------------------------------------------------------------------------------------------------------------------------------------------------
            case gv.CMD.MAP_MOVE_ITEM:
                if (packet.id == "" || packet.id == null || packet.id == undefined) {
                    serverCallback.callErrString("Không thể di chuyển nhà!");
                } else
                    serverCallback.callMoveItem(packet.id, packet.x, packet.y);
                break;

            case gv.CMD.MAP_CREATE_ITEM:
                cc.log("CREATE ITEM: " + packet.id + packet.time + packet.x + packet.y + packet.type);
                if (packet.time < 0 || packet.id === "") {
                    serverCallback.callErrString("Khởi tạo không thành công!");
                    break;
                }
                if (packet.id && packet.time && packet.x && packet.y && packet.type)
                    serverCallback.callCreateBuilding(packet.id, packet.type, packet.x, packet.y, packet.time);
                break;
            case gv.CMD.MAP_UPGRADE_ITEM:
                cc.log("Upgrade ITEM: " + packet.time + " " + packet.id);
                if (Util.parseInt(packet.time + "") < 0 || packet.id === "") {
                    serverCallback.callErrString("Nâng cấp không thành công!");
                    break;
                }

                serverCallback.callUpgradeBuilding(packet.id, packet.time);

                break;
            case gv.CMD.MAP_GET:
                cc.log("MAP ITEM: " + packet.data);

                gv.userMap = packet.data;
                this.sendGetObs();
                break;
            case gv.CMD.OBS_GET:
                cc.log("OBS ITEM: " + packet.data);

                gv.userObs = packet.data;

                // this.sendClearOBS("20"+"");
                fr.getCurrentScreen().onFinishLogin();

                break;
            case gv.CMD.MAP_CLEAR_OBS:
                cc.log("OBS ITEM: " + packet.id + " " + packet.time);
                if (packet.time < 0 || packet.id === "") {
                    serverCallback.callErrString("Dọn dẹp không thành công!");
                    break
                }
                serverCallback.callRemoveObs(packet.id, packet.time);

                break;
            case gv.CMD.MAP_QUICK_FINISH:
                cc.log("MAP_QUICK_FINISH: " + packet.id + " " + packet.level);
                if (packet.time < 0 || packet.level < 0) {
                    serverCallback.callErrString("Không thành công!");
                    break
                }


                else if (Util.parseInt(packet.id) > MAXOBSID) {
                    cc.log("BUILDING  FINISH");
                    serverCallback.callUpgradeBuildingFinish(packet.id, packet.level);
                }
                else if (Util.parseInt(packet.id) > 0) {
                    cc.log("OBS FINISH");
                    serverCallback.callRemoveObsFinish(packet.id);
                }

                break;
            case gv.CMD.MAP_CANCLE:

                cc.log("MAP_CANCLE @: " + packet.id + " " + packet.level);
                if (packet.time < 0 || packet.level < 0) {
                    serverCallback.callErrString("Hủy không thành công!");
                    break;
                }


                else if (Util.parseInt(packet.id) > MAXOBSID) {
                    cc.log("BUILDING  FINISH");
                    serverCallback.callUpgradeBuildingFinish(packet.id, packet.level);
                }
                break;

        }
    },

    sendUpDateLobby: function () {
        cc.log("sendUpDateLobby");
        var pk = this.gameClient.getOutPacket(CmdSendUpDateLobby);
        pk.pack();
        this.gameClient.sendPacket(pk);
    },
    sendUpDateLobbyMax: function () {
        cc.log("sendUpDateLobbyMax");
        var pk = this.gameClient.getOutPacket(CmdSendUpDateLobbyMax);
        pk.pack();
        this.gameClient.sendPacket(pk);
    },

    sendGetUserInfo: function () {
        cc.log("sendGetUserInfo");
        var pk = this.gameClient.getOutPacket(CmdSendUserInfo);
        pk.pack();
        this.gameClient.sendPacket(pk);
    },
    sendLoginRequest: function (user) {
        cc.log("sendLoginRequest " + user);
        if (user == null || user == "" || user == undefined) return;
        var pk = this.gameClient.getOutPacket(CmdSendLogin);
        pk.pack(user);
        this.gameClient.sendPacket(pk);
    },
    sendMapMoveItem: function (idbuild, posX, posY) {
        cc.log("Send Move: " +
            idbuild + " " + posX + " " + posY
        );
        var pk = this.gameClient.getOutPacket(CmdSendMove);

        pk.pack(idbuild, posX, posY);
        this.gameClient.sendPacket(pk);
    },
    sendMapCreateItem: function (idbuild, posX, posY) {
        cc.log("Send CreateItem: " +
            idbuild + " " + posX + " " + posY
        );
        var pk = this.gameClient.getOutPacket(CmdSendCreate);

        pk.pack(idbuild, posX, posY);
        this.gameClient.sendPacket(pk);
    },
    sendMapupgradeItem: function (idbuild) {
        // idbuild = "100";
        cc.log("sendMapupgradeItem: " +
            idbuild
        );
        // this.sendMapCreateItem("RES_1",1,1);
        var pk = this.gameClient.getOutPacket(CmdSendUpgrade);

        pk.pack(idbuild);
        this.gameClient.sendPacket(pk);

    },
    sendGetUserRes: function () {
        var pk = this.gameClient.getOutPacket(CmdSendUserRES);
        pk.pack();
        this.gameClient.sendPacket(pk);
    },
    sendGetMap: function () {
        cc.log("sendGetMap");
        var pk = this.gameClient.getOutPacket(CmdSendMap);
        pk.pack();
        this.gameClient.sendPacket(pk);
    },
    sendGetObs: function () {
        cc.log("sendGetObs");
        var pk = this.gameClient.getOutPacket(CmdSendObs);
        pk.pack();
        this.gameClient.sendPacket(pk);
    },
    sendClearOBS: function (id) {

        cc.log("sendClearOBS: " +
            id
        );
        var pk = this.gameClient.getOutPacket(CmdSendClearObs);

        pk.pack(id + "");
        this.gameClient.sendPacket(pk);

    },

    sendQuickFinishBuilding: function (id) {
        cc.log("send Quick finish Building: " + id);

        var pk = this.gameClient.getOutPacket(CmdSendQuickFinish);
        pk.pack(id + "");
        this.gameClient.sendPacket(pk);
    },

    sendCancelUpgradeBuilding: function (id) {
        cc.log("Send cancel upgrade Building: " + id);
        var pk = this.gameClient.getOutPacket(CmdSendCancelUpgrade);
        pk.pack(id);
        this.gameClient.sendPacket(pk);
    },


    sendFinishMapAction: function (id) {
        cc.log("sendFinishMapAction: " + id);
        var pk = this.gameClient.getOutPacket(CmdSendFinish);
        pk.pack(id);
        this.gameClient.sendPacket(pk);
    },
    sendCheatLobby: function () {
        ;
        var pk = this.gameClient.getOutPacket(CmdSendCheatLobby);
        pk.pack(10000000, 10000000, 10000000, 10000000);
        this.gameClient.sendPacket(pk);
    },
    sendTradeResource: function (data, type) {// type 1:gold   2:elixir   3:darkElixir
        cc.log("sendTradeResource: ", data, type);
        var pk = this.gameClient.getOutPacket(CmdSendTradeResource);
        pk.pack(data, type);
        this.gameClient.sendPacket(pk);
    },
    sendFreeTheMason: function () {// type 1:gold   2:elixir   3:darkElixir
        cc.log("sendFreeTheMason: ");
        var pk = this.gameClient.getOutPacket(CmdSendFreeTheMason);
        pk.pack();
        this.gameClient.sendPacket(pk);
    }


});



