gv.CMD = gv.CMD || {};
gv.CMD.HAND_SHAKE = 0;
gv.CMD.USER_LOGIN = 1;

gv.CMD.USER_INFO = 1014;
gv.CMD.USER_RES = 1015;
gv.CMD.MOVE = 2001;
gv.CMD.USER_UPDATE_LOBBY = 1016;
gv.CMD.UPDATE_USER_INFO = 1017;
gv.CMD.USER_UPDATE_LOBBY_MAX = 1018;
gv.CMD.USER_UPDATE_CHEAT_LOBBY = 1019;
gv.CMD.USER_TRADE_RESOURCE = 1020;


gv.CMD.MAP_GET = 3001;
gv.CMD.OBS_GET = 3005;
gv.CMD.MAP_MOVE_ITEM = 3002;

gv.CMD.MAP_CREATE_ITEM = 3004;
gv.CMD.MAP_UPGRADE_ITEM = 3006;
gv.CMD.MAP_CLEAR_OBS = 3007;
gv.CMD.MAP_QUICK_FINISH = 3008;
gv.CMD.MAP_CANCLE = 3009;
gv.CMD.FINISH = 3010;
gv.CMD.FREE_THE_MASON = 3011;

testnetwork = testnetwork || {};
testnetwork.packetMap = {};

/** Outpacket */

//Handshake
CmdSendHandshake = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setControllerId(gv.CONTROLLER_ID.SPECIAL_CONTROLLER);
            this.setCmdId(gv.CMD.HAND_SHAKE);
        },
        putData: function () {
            //pack
            this.packHeader();
            //update
            this.updateSize();
        }
    }
)
CmdSendUserInfo = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.USER_INFO);
        },
        pack: function () {
            this.packHeader();
            this.updateSize();
        }
    }
)
CmdSendUpDateLobby = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.USER_UPDATE_LOBBY);
        },
        pack: function () {
            this.packHeader();
            this.updateSize();
        }
    }
)
CmdSendUpDateLobbyMax = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.USER_UPDATE_LOBBY_MAX);
        },
        pack: function () {
            this.packHeader();
            this.updateSize();
        }
    }
)
CmdSendMap = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.MAP_GET);
        },
        pack: function () {
            this.packHeader();
            this.updateSize();
        }
    }
)
CmdSendObs = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.OBS_GET);
        },
        pack: function () {
            this.packHeader();
            this.updateSize();
        }
    }
)
CmdSendUserRES = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.USER_RES);
        },
        pack: function () {
            this.packHeader();
            this.updateSize();
        }
    }
)
CmdSendLogin = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.USER_LOGIN);
        },
        pack: function (user) {
            this.packHeader();
            this.putString(user);
            this.updateSize();
        }
    }
)

CmdSendMove = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.MAP_MOVE_ITEM);
        },
        pack: function (idBuild, posX, posY) {
            this.packHeader();
            this.putString(idBuild);
            this.putShort(posX);
            this.putShort(posY);
            this.updateSize();
        }
    }
)


CmdSendCreate = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.MAP_CREATE_ITEM);
        },
        pack: function (idBuild, posX, posY) {
            this.packHeader();
            this.putString(idBuild);
            this.putInt(posX);
            this.putInt(posY);
            this.updateSize();
        }
    }
)

CmdSendUpgrade = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.MAP_UPGRADE_ITEM);
        },
        pack: function (idBuild) {
            this.packHeader();
            this.putString(idBuild);
            this.updateSize();
        }
    }
)

CmdSendClearObs = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.MAP_CLEAR_OBS);
        },
        pack: function (id) {
            this.packHeader();
            this.putString(id);
            this.updateSize();
        }
    }
)
CmdSendQuickFinish = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.MAP_QUICK_FINISH);
        },
        pack: function (id) {
            this.packHeader();
            this.putString(id);
            this.updateSize();
        }
    }
)
CmdSendCancelUpgrade = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.MAP_CANCLE);
        },
        pack: function (id) {
            this.packHeader();
            this.putString(id);
            this.updateSize();
        }
    }
)
CmdSendFinish = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.FINISH);
        },
        pack: function (id) {
            this.packHeader();
            this.putString(id);
            this.updateSize();
        }
    }
)
CmdSendCheatLobby = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.USER_UPDATE_CHEAT_LOBBY);
        },
        pack: function (gold, elixir, darkelixir, coin) {
            this.packHeader();
            this.putInt(gold);
            this.putInt(elixir);
            this.putInt(darkelixir);
            this.putInt(coin);


            this.updateSize();
        }
    }
)
CmdSendTradeResource = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.USER_TRADE_RESOURCE);
        },
        pack: function (data, type) {
            this.packHeader();
            this.putInt(data);
            this.putInt(type);


            this.updateSize();
        }
    }
)
CmdSendTradeResource = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.USER_TRADE_RESOURCE);
        },
        pack: function (data, type) {
            this.packHeader();
            this.putInt(data);
            this.putInt(type);


            this.updateSize();
        }
    }
)

CmdSendFreeTheMason = fr.OutPacket.extend(
    {
        ctor: function () {
            this._super();
            this.initData(100);
            this.setCmdId(gv.CMD.FREE_THE_MASON);
        },
        pack: function (data, type) {
            this.packHeader();

            this.updateSize();
        }
    }
)
/**
 * InPacket
 */

//Handshake
testnetwork.packetMap[gv.CMD.HAND_SHAKE] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {
            this.token = this.getString();
        }
    }
);

testnetwork.packetMap[gv.CMD.USER_LOGIN] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {
        }
    }
);


testnetwork.packetMap[gv.CMD.USER_INFO] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {

            var json = this.getString();
            cc.log("User info");
            cc.log(json);
            this.data = JSON.parse(json);

        }
    }
);

testnetwork.packetMap[gv.CMD.USER_UPDATE_LOBBY] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {


            this.gold = this.getInt();
            this.elixir = this.getInt();
            this.darkElixir = this.getInt();
            this.coin = this.getInt();

            this.builder = this.getInt();
            this.level = this.getInt();
            this.trophy = this.getInt();
            this.army = this.getInt();
            cc.log(this.gold + " " + this.elixir + " " + this.coin + " " + this.darkElixir + " " + this.builder + " " + this.level + " " + this.trophy + " " + this.army);

        }
    }
);
testnetwork.packetMap[gv.CMD.USER_UPDATE_LOBBY_MAX] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {
            this.gold = this.getInt();
            this.elixir = this.getInt();
            this.darkElixir = this.getInt();
            this.coin = this.getInt();

            this.builder = this.getInt();
            this.level = this.getInt();
            this.trophy = this.getInt();
            this.army = this.getInt();


        }
    }
);


testnetwork.packetMap[gv.CMD.MAP_GET] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {

            var json = this.getString();
            cc.log("User map");
            cc.log(json);
            this.data = JSON.parse(json);

        }
    }
);

testnetwork.packetMap[gv.CMD.OBS_GET] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {

            var json = this.getString();
            cc.log("USer obs");
            cc.log(json);
            this.data = JSON.parse(json);

        }
    }
);
testnetwork.packetMap[gv.CMD.USER_RES] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {

            var json = this.getString()
            cc.log(json);
            this.data = JSON.parse(json);
        }
    }
);
testnetwork.packetMap[gv.CMD.MAP_MOVE_ITEM] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {

            this.id = this.getString();
            this.x = this.getShort();
            this.y = this.getShort();

        }
    }
);
testnetwork.packetMap[gv.CMD.MAP_CREATE_ITEM] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {
            this.id = this.getString();
            this.type = this.getString();
            this.time = this.getLong();
            this.x = this.getInt();
            this.y = this.getInt();

        }
    }
);

testnetwork.packetMap[gv.CMD.MAP_UPGRADE_ITEM] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {
            this.id = this.getString();
            this.time = this.getLong();

        }
    }
);


testnetwork.packetMap[gv.CMD.MAP_CLEAR_OBS] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {

            this.id = this.getString();
            this.time = this.getLong();

        }
    }
);

testnetwork.packetMap[gv.CMD.MAP_QUICK_FINISH] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {

            this.id = this.getString();
            this.level = this.getInt();
            Util.log("MAP_QUICK_FINISH", this.id + " " + this.level);

        }
    }
);
testnetwork.packetMap[gv.CMD.MAP_CANCLE] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {

            this.id = this.getString();
            this.level = this.getInt();
            Util.log("MAP_CANCLE", this.id + " " + this.level);

        }
    }
);
testnetwork.packetMap[gv.CMD.FINISH] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {


        }
    }
);
testnetwork.packetMap[gv.CMD.USER_UPDATE_CHEAT_LOBBY] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {


        }
    }
);
testnetwork.packetMap[gv.CMD.USER_TRADE_RESOURCE] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {


        }
    }
);
testnetwork.packetMap[gv.CMD.FREE_THE_MASON] = fr.InPacket.extend(
    {
        ctor: function () {
            this._super();
        },
        readData: function () {


        }
    }
);




